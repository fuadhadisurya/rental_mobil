<?php
	include('head.html');
	include('logoheader.html');
?>

			<!-- start banner Area -->
			<section class="banner-area relative" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								About Us				
							</h1>	
							<p class="text-white link-nav"><a href="index.html">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="about.html"> About Us</a></p>
						</div>	
					</div>
				</div>
			</section>
			<!-- End banner Area -->	

			<!-- Start home-about Area -->
			<section class="home-about-area section-gap aboutus-about" id="about">
				<div class="container">
					<div class="row justify-content-center align-items-center">
						<div class="col-lg-8 col-md-12 home-about-left">
							<h1>
								Profil Perusahaan
							</h1>
							<p class="pb-20">
								Wonderful Travel merupakan salah satu penyedia layanan rental mobil di 
								Indramayu. Menggunakan armada mobil keluaran baru, dengan kondisi yang sangat 
								terawat untuk disewakan kepada Anda. Pilihan mobil yang kami sediakan 
								sangat bervariatif dan harga yang sangat kompetitif di Indramayu. Kami 
								menawarkan untuk Anda macam-macam paket perjalanan Travel baik dalam kota atau 
								luar kota. Pelayanan dari pegawai kami yang ramah
								Anda juga dapat menyewa dengan supir ataupun tanpa supir.
							</p>
								<p class="sub">Visi</p>
								<p class="pb-20">
									Menjadi perusahaan jasa transportasi yang menyediakan 
									jasa penyewaan mobil yang dapat dipercaya. Kami berupaya terus untuk menempatkan 
									diri sebagai perusahaan rental mobil terdepan di kawasan Indramayu
								</p>
								<p class="sub">Misi</p>
								</p clas="pb-20">
									Kami selalu berusaha ada untuk menyediakan kendaraan yang terbaru dan optimal 
									untuk perjalanan anda dan kami akan selalu membuat klien kami puas dengan jasa yang diberikan.
								</p>
						</div>
					</div>
				</div>	
			</section>
			<!-- End home-about Area -->
				
			<!-- Start cat Area -->
			<section class="cat-area section-gap aboutus-cat" id="feature">
				<div class="container">							
					<div class="row">
						<div class="col-lg-4">	
							<div class="single-cat d-flex flex-column">
								<a href="#" class="hb-sm-margin mx-auto d-block"><span class="hb hb-sm inv hb-facebook-inv"><span class="lnr lnr-cog"></span></span></a>
								<h4 class="mb-20" style="margin-top: 23px;">Armada Mobil</h4>
								<p>
								Kami selalu melakukan perawatan secara berkala pada armada mobil kami, untuk menjaga kenyamanan berkendara anda selama diperjalanan.
								</p>
							</div>															
						</div>
						<div class="col-lg-4">	
							<div class="single-cat">
								<a href="#" class="hb-sm-margin mx-auto d-block"><span class="hb hb-sm inv hb-facebook-inv"><span class="lnr lnr-star"></span></span></a>
								<h4 class="mt-40 mb-20">Quality Over Value</h4>
								<p>
								Kepuasan anda adalah Prioritas kami.
								</p>
							</div>															
						</div>
						<div class="col-lg-4">
							<div class="single-cat">
								<a href="#" class="hb-sm-margin mx-auto d-block"><span class="hb hb-sm inv hb-facebook-inv"><span class="lnr lnr-car"></span></span></a>
								<h4 class="mt-40 mb-20">Mobil yang Bervariasi</h4>
								<p>
								Menyediakan beberapa pilihan mobil yang menyesuaikan dengan kebutuhan rental dan keuangan anda.
								</p>
							</div>							
						</div>
					</div>
				</div>	
			</section>
			<!-- End cat Area -->						

			<!-- Start faq Area -->
			<section class="faq-area section-gap relative">
				<div class="overlay overlay-bg"></div>
				<div class="container">
					<div class="row justify-content-center align-items-center">
					<?php
                        include('akun/dist/include/connect.php');
                    ?>
						<div class="col-lg-3 col-md-6">
							<div class="single-faq">
								<div class="circle">
								  <div class="inner"></div>
								</div>
								<h5 span class="counter">
									<?php 
										$total_user=mysqli_query($conn,"SELECT * FROM user");
										echo mysqli_num_rows($total_user);
									?>
								</h5>
								<p>
									Total Pelanggan 
								</p>
							</div>
						</div>
						<div class="col-lg-3 col-md-6">
							<div class="single-faq">
								<div class="circle">
								  <div class="inner"></div>
								</div>
								<h5 class="counter">
									<?php 
										$total_mobil=mysqli_query($conn,"SELECT * FROM mobil");
										echo mysqli_num_rows($total_mobil);
									?>
								</h5>
								<p>
									Total Mobil
								</p>
							</div>
						</div>
						<div class="col-lg-3 col-md-6">
							<div class="single-faq">
								<div class="circle">
								  <div class="inner"></div>
								</div>
								<h5 class="counter">
									<?php 
										$mobil_tersedia=mysqli_query($conn,"SELECT * FROM mobil WHERE status='Tersedia'");
										echo mysqli_num_rows($mobil_tersedia);
									?>
								</h5>
								<p>
									Mobil Tersedia
								</p>
							</div>
						</div>
						<div class="col-lg-3 col-md-6">
							<div class="single-faq">
								<div class="circle">
								  <div class="inner"></div>
								</div>
								<h5 class="counter">
									<?php 
										$transaksi=mysqli_query($conn,"SELECT * FROM transaksi WHERE status_pembayaran='Selesai'");
										echo mysqli_num_rows($transaksi);
									?>
								</h5>
								<p>
									Transaksi Berhasil
								</p>
							</div>
						</div>																		
					</div>
				</div>	
			</section>
			<!-- End faq Area -->
				
			<?php
include('footer.html');
?>