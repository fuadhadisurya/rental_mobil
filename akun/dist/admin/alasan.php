<?php
include('../include/headeradmin.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include '../include/connect.php';

if(isset($_POST['submit'])) {
    $id=$_POST['id'];
    $id_mobil=$_POST['id_mobil'];
    $status_pembayaran=$_POST['status_pembayaran'];
    $alasan = $_POST['alasan'];
    $status_kendaraan=$_POST['status_kendaraan'];

    $query_mobil="UPDATE mobil SET status='$status_kendaraan' WHERE id='$id_mobil'";
    $result_mobil=mysqli_query($conn, $query_mobil) or die (mysqli_error($conn));
    
    $query="UPDATE transaksi SET status_pembayaran='$status_pembayaran',alasan='$alasan' WHERE id='$id'";
    $result=mysqli_query($conn, $query) or die (mysqli_error($conn));
    if( $result && $result_mobil ) {
        // kalau berhasil alihkan ke halaman list-siswa.php
        echo '<script> alert ("Berhasil dibatalkan");</script>';
        echo '<script>window.location.assign("../admin/konfirmasi_peminjaman.php")</script>';
    } else {
        // kalau gagal tampilkan pesan
        echo '<script> alert ("gagal dilakukan");</script>';
        echo '<script>window.location.assign("alasan.php")</script>';
    }
    
}
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Alasan penolakan transaksi</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="../admin/index.php">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="../admin/konfirmasi_pembayaran.php">Konfirmasi pembayaran</a></li>
                            <li class="breadcrumb-item active">Alasan penolakan transaksi</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                            <?php
                            //untuk menampilkan data sebelum di update/diedit
                            $id = $_GET['id'];
                            $result = mysqli_query($conn, "SELECT * FROM transaksi WHERE id = '$id'");
                            $user = mysqli_fetch_array($result);
                            ?>
                            <form method="post">
                                <input type="hidden" name="id_mobil" value="<?php echo $user['id_mobil'] ?>">
                                <input type="hidden" value="Tersedia" name="status_kendaraan">
                                <input type="hidden" name="id" value="<?php echo $user['id'] ?>">
                                <input type="hidden" name="status_pembayaran" value="Ditolak">
                                <div class="form-group">
                                    <label class="small mb-1" for="inputAlasan">Alasan</label>
                                    <textarea class="form-control py-4" rows="10" name="alasan" id="inputAlasan" type="text" placeholder="Masukkan keterangan penolakan" required="required" autofocus="autofocus"></textarea>
                                </div>
                                <div class="form-group mt-4 mb-0">
                                    <button class="btn btn-primary btn-block" type="submit" name="submit">Kirim</button>
                                    <a href="../admin/konfirmasi_peminjaman.php" class="btn btn-secondary btn-block">Kembali</a>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </main>
<?php
    include('../include/footer.html');
?>