<?php
include('../include/headeradmin.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Data mobil</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item active">Data mobil</li>
                        </ol>

                        <div class="card mb-4">
                            <div class="card-header"><i class="fas fa-table mr-1"></i>Data mobil</div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="mobil" width="100%" cellspacing="0">
                                        <thead>
                                            <tr class="text-center">
                                                <th>No.</th>
                                                <th>Nama</th>
                                                <th>Transmisi</th>
                                                <th>No. Polisi</th>
                                                <th>Harga sewa</th>
                                                <th>status</th>
                                                <th>Foto</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                                include('../include/connect.php');
                                                $query=mysqli_query($conn,"SELECT * FROM mobil");
                                                while($row=mysqli_fetch_array($query)){
                                                    $harga=$row['harga_sewa'];
                                            ?>
                                                        <tr>
                                                            <td><?php echo $row['id'] ?></td>
                                                            <td><?php echo $row['nama'] ?></td>
                                                            <td><?php echo $row['transmisi'] ?></td>
                                                            <td><?php echo $row['no_polisi'] ?></td>
                                                            <td>Rp. <?php echo number_format($harga, 0, ",",",") ?></td>
                                                            <td>
                                                                <?php if($row['status']=="Tersedia"){ ?>
                                                                    <span class="badge badge-success">Tersedia</span>
                                                                <?php
                                                                } elseif($row['status']=="Tidak tersedia"){ ?>
                                                                    <span class="badge badge-danger">Tidak tersedia</span>
                                                                <?php } ?>
                                                            </td>
                                                            <td><img src="<?php echo "../image/mobil/".$row['foto'];?>"width="200px"></td>
                                                            <td>
                                                                <a href="detail_mobil.php?id=<?php echo $row["id"]?>" class="btn btn-primary"><i class="fas fa-info-circle"></i></a>
                                                                <a href="edit_mobil.php?id=<?php echo $row["id"]?>" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                                                                <a onclick="return confirm('Are you want deleting data')" href="hapus_mobil.php?id=<?php echo $row["id"]?>" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                                            </td>
                                                        </tr>
                                            <?php
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <script>
                $(document).ready(function() {
                    $('#mobil').DataTable();
                } );
                </script>
<?php
    include('../include/footer.html');
?>