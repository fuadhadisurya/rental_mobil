<?php
include('../include/headeradmin.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Data Pelanggan</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item active">Data Pelanggan</li>
                        </ol>

                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>Data pelanggan Offline
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="offline" width="100%" cellspacing="0">
                                        <thead>
                                            <tr class="text-center">
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>NIK</th>
                                                <th>No. HP</th>
                                                <th>Email</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                include('../include/connect.php');
                                                $query=mysqli_query($conn,"SELECT * FROM user_offline");
                                                while($row=mysqli_fetch_array($query)){
                                                ?>
                                                        <tr>
                                                            <td><?php echo $row['id'] ?></td>
                                                            <td><?php echo $row['nama'] ?></td>
                                                            <td><?php echo $row['nik'] ?></td>
                                                            <td><?php echo $row['no_hp'] ?></td>
                                                            <td><?php echo $row['email'] ?></td>
                                                            <td>
                                                                <a href="detail_user_offline.php?id=<?php echo $row["id"]?>" class="btn btn-primary"><i class="fas fa-info-circle"></i></a>
                                                                <a href="edit_pelanggan.php?id=<?php echo $row["id"]?>" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                                                                <a onclick="return confirm('Are you want deleting data')" href="hapus_pelanggan.php?id=<?php echo $row["id"]?>" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                                            </td>
                                                        </tr>
                                                <?php 
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>Data pelanggan Online
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="online" width="100%" cellspacing="0">
                                        <thead>
                                            <tr class="text-center">
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>NIK</th>
                                                <th>No. HP</th>
                                                <th>Email</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                include('../include/connect.php');
                                                $query=mysqli_query($conn,"SELECT * FROM user WHERE level='pelanggan'");
                                                while($row=mysqli_fetch_array($query)){
                                            ?>
                                                <tr>
                                                    <td><?php echo $row['id'] ?></td>
                                                    <td><?php echo $row['nama'] ?></td>
                                                    <td><?php echo $row['nik'] ?></td>
                                                    <td><?php echo $row['no_hp'] ?></td>
                                                    <td><?php echo $row['email'] ?></td>
                                                    <td>
                                                        <a href="detail_user.php?id=<?php echo $row["id"]?>" class="btn btn-primary"><i class="fas fa-info-circle"></i></a>
                                                    </td>
                                                </tr>
                                            <?php
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <script>
                $(document).ready(function() {
                    $('#offline').DataTable();
                } );
                </script>
                <script>
                $(document).ready(function() {
                    $('#online').DataTable();
                } );
                </script>
<?php
    include('../include/footer.html');
?>