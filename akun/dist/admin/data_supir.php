<?php
include('../include/headeradmin.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Data supir</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item active">Data supir</li>
                        </ol>

                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>Data supir
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="supir" width="100%" cellspacing="0">
                                        <thead>
                                            <tr class="text-center">
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>NIK</th>
                                                <th>No. HP</th>
                                                <th>Email</th>
                                                <th>Alamat</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                include('../include/connect.php');
                                                $query=mysqli_query($conn,"SELECT * FROM supir");
                                                while($row=mysqli_fetch_array($query)){
                                            ?>
                                                        <tr>
                                                            <td><?php echo $row['id'] ?></td>
                                                            <td><?php echo $row['nama'] ?></td>
                                                            <td><?php echo $row['nik'] ?></td>
                                                            <td><?php echo $row['no_hp'] ?></td>
                                                            <td><?php echo $row['email'] ?></td>
                                                            <td><?php echo $row['alamat'] ?></td>
                                                            <td>
                                                                <?php if($row['status']=="Tersedia"){ ?>
                                                                    <span class="badge badge-success">Tersedia</span>
                                                                <?php
                                                                } elseif($row['status']=="Tidak tersedia"){ ?>
                                                                    <span class="badge badge-danger">Tidak tersedia</span>
                                                                <?php } ?>
                                                            </td>
                                                            <td>
                                                                <a href="edit_supir.php?id=<?php echo $row["id"]?>" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                                                                <a onclick="return confirm('Are you want deleting data')" href="hapus_supir.php?id=<?php echo $row["id"]?>" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                                            </td>
                                                        </tr>
                                            <?php
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <script>
                $(document).ready(function() {
                    $('#supir').DataTable();
                } );
                </script>
<?php
    include('../include/footer.html');
?>