<?php
include('../include/headeradmin.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include '../include/connect.php';
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Detail mobil</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="data_mobil.php">Data mobil</a></li>
                            <li class="breadcrumb-item active">Detail mobil</li>
                        </ol>
                        
                        <div class="card mb-4">
                            <div class="card-body">
                            <table>
                                    <?php
                                    include('../include/connect.php');
                                    $id=$_GET['id'];
                                    $query=mysqli_query($conn,"SELECT * FROM mobil WHERE id='$id'");
                                    while($row=mysqli_fetch_array($query)){
                                        $harga=$row['harga_sewa'];
                                    ?>
                                    <tr>
                                        <td>ID</td>
                                        <td> : </td>
                                        <td><?php echo $row['id'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama</td>
                                        <td> : </td>
                                        <td><?php echo $row['nama'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>No Polisi</td>
                                        <td> : </td>
                                        <td><?php echo $row['no_polisi'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Transmisi</td>
                                        <td> : </td>
                                        <td><?php echo $row['transmisi'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Jenis</td>
                                        <td> : </td>
                                        <td><?php echo $row['jenis'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Harga sewa</td>
                                        <td> : </td>
                                        <td>Rp. <?php echo number_format($harga, 0, ",",","); ?>/Hari</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td> : </td>
                                        <td>
                                            <?php if($row['status']=="Tersedia"){ ?>
                                                <span class="badge badge-success">Tersedia</span>
                                            <?php
                                            } elseif($row['status']=="Tidak tersedia"){ ?>
                                                <span class="badge badge-danger">Tidak tersedia</span>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Foto</td>
                                        <td> : </td>
                                    </tr>
                                </table>
                                <img src="../image/mobil/<?php echo $row['foto'] ?>" alt="Bukti transfer">
                                <?php } ?>
                            </div>
                            <div class="card-footer">
                                <a href="data_mobil.php" class="btn btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
                </main>
                <script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
<?php
    include('../include/footer.html');
?>