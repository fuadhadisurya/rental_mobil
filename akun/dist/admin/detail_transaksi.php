<?php
include('../include/headeradmin.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include '../include/connect.php';
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Detail transaksi</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="menu_transaksi.php">Menu transaksi</a></li>
                            <li class="breadcrumb-item active">Detail transaksi</li>
                        </ol>
                        
                        <div class="card mb-4">
                            <div class="card-body">
                            <table>
                                    <?php
                                    include('../include/connect.php');
                                    $id=$_GET['id'];
                                    $query=mysqli_query($conn,"SELECT *, mobil.nama AS nama_mobil, user.nama AS nama_user, transaksi.id AS id_pesanan, user_offline.nama AS nama_pelanggan, transaksi.foto AS bukti_transaksi, transaksi.id AS id_transaksi,
                                    user.nik AS nik_online, user_offline.nik AS nik_offline, user.alamat AS alamat_online, user_offline.alamat AS alamat_offline, user.no_hp AS no_hp_online, user_offline.no_hp AS no_hp_offline, user.email AS email_online, user_offline.email AS email_offline, user.jenis_kelamin AS jenis_kelamin_online, user_offline.jenis_kelamin AS jenis_kelamin_offline
                                    FROM transaksi  INNER JOIN mobil ON transaksi.id_mobil=mobil.id LEFT JOIN user ON transaksi.id_user=user.id LEFT JOIN user_offline ON transaksi.id_user_offline=user_offline.id WHERE transaksi.id='$id'");
                                    while($row=mysqli_fetch_array($query)){
                                        $harga=$row['harga_sewa'];
                                        $total=$row['total_pembayaran'];
                                        $denda=$row['denda'];
                                    ?>
                                    <tr>
                                        <td>ID Booking</td>
                                        <td> : </td>
                                        <td><?php echo $row['id_transaksi'] ?></td>
                                    </tr>
                                    <?php if($row['nama_user']==true){?>
                                    <tr>
                                        <td>Nama</td>
                                        <td> : </td>
                                        <td><?php echo $row['nama_user'] ?></td>
                                    </tr>
                                    <?php } else { ?>
                                    <tr>
                                        <td>Nama</td>
                                        <td> : </td>
                                        <td><?php echo $row['nama_pelanggan'] ?></td>
                                    </tr>
                                    <?php } ?>
                                    <?php if($row['nama_user']==true){?>
                                    <tr>
                                        <td>NIK</td>
                                        <td> : </td>
                                        <td><?php echo $row['nik_online'] ?></td>
                                    </tr>
                                    <?php } else { ?>
                                    <tr>
                                        <td>NIK</td>
                                        <td> : </td>
                                        <td><?php echo $row['nik_offline'] ?></td>
                                    </tr>
                                    <?php } ?>
                                    <?php if($row['nama_user']==true){?>
                                    <tr>
                                        <td>Alamat</td>
                                        <td> : </td>
                                        <td><?php echo $row['alamat_online'] ?></td>
                                    </tr>
                                    <?php } else { ?>
                                    <tr>
                                        <td>Alamat</td>
                                        <td> : </td>
                                        <td><?php echo $row['alamat_offline'] ?></td>
                                    </tr>
                                    <?php } ?>
                                    <?php if($row['nama_user']==true){?>
                                    <tr>
                                        <td>No HP</td>
                                        <td> : </td>
                                        <td><?php echo $row['no_hp_online'] ?></td>
                                    </tr>
                                    <?php } else { ?>
                                    <tr>
                                        <td>No. HP</td>
                                        <td> : </td>
                                        <td><?php echo $row['no_hp_offline'] ?></td>
                                    </tr>
                                    <?php } ?>
                                    <?php if($row['nama_user']==true){?>
                                    <tr>
                                        <td>Email</td>
                                        <td> : </td>
                                        <td><?php echo $row['email_online'] ?></td>
                                    </tr>
                                    <?php } else { ?>
                                    <tr>
                                        <td>Email</td>
                                        <td> : </td>
                                        <td><?php echo $row['email_offline'] ?></td>
                                    </tr>
                                    <?php } ?>
                                    <?php if($row['nama_user']==true){?>
                                    <tr>
                                        <td>Jenis Kelamin</td>
                                        <td> : </td>
                                        <td><?php echo $row['jenis_kelamin_online'] ?></td>
                                    </tr>
                                    <?php } else { ?>
                                    <tr>
                                        <td>Jenis Kelamin</td>
                                        <td> : </td>
                                        <td><?php echo $row['jenis_kelamin_offline'] ?></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td>Nama mobil</td>
                                        <td> : </td>
                                        <td><?php echo $row['nama_mobil'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>No. Polisi</td>
                                        <td> : </td>
                                        <td><?php echo $row['no_polisi'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal ambil</td>
                                        <td> : </td>
                                        <td><?php echo $row['tgl_sewa'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Jatuh tempo</td>
                                        <td> : </td>
                                        <td><?php echo $row['jatuh_tempo'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal kembali</td>
                                        <td> : </td>
                                        <td><?php echo $row['tgl_kembali'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Lama sewa</td>
                                        <td> : </td>
                                        <td><?php echo $row['lama'] ?> Hari</td>
                                    </tr>
                                    <tr>
                                        <td>Supir</td>
                                        <td> : </td>
                                        <td><?php echo $row['supir'] ?></td>
                                    </tr>
                                    <?php if($row['supir']=='Ya'){ ?>
                                    <tr>
                                        <td>ID Supir</td>
                                        <td> : </td>
                                        <td><?php echo $row['id_supir'] ?></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td>Jaminan</td>
                                        <td> : </td>
                                        <td><?php echo $row['jaminan'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Harga sewa</td>
                                        <td> : </td>
                                        <td>Rp. <?php echo number_format($harga, 0, ",",","); ?>/Hari</td>
                                    </tr>
                                    <?php if($row['supir']=='Ya'){ ?>
                                    <tr>
                                        <td>Jasa supir</td>
                                        <td> : </td>
                                        <td>Rp. 100.000/Hari</td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td>Total pembayaran</td>
                                        <td> : </td>
                                        <td>Rp. <?php echo number_format($total, 0, ",",","); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal pesan</td>
                                        <td> : </td>
                                        <td><?php echo $row['tanggal_pesan'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Denda</td>
                                        <td> : </td>
                                        <td>Rp. <?php echo number_format($denda, 0, ",",","); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Keterangan</td>
                                        <td> : </td>
                                        <td><?php echo $row['alasan'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Bukti pembayaran</td>
                                        <td> : </td>
                                    </tr>
                                </table>
                                <img src="../image/bukti_transfer/<?php echo $row['bukti_transaksi'] ?>" alt="Bukti Pembayaran (Online User only)" width="950px"">
                                <?php } ?>
                            </div>
                            <div class="card-footer">
                                <a href="menu_transaksi.php" class="btn btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
                </main>
                <script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
<?php
    include('../include/footer.html');
?>