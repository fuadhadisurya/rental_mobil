<?php
include('../include/headeradmin.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include '../include/connect.php';
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Detail transaksi</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="transaksi.php">Transaksi</a></li>
                            <li class="breadcrumb-item active">Detail transaksi</li>
                        </ol>
                        
                        <div class="card mb-4">
                            <div class="card-body">
                            <table>
                                    <?php
                                    include('../include/connect.php');
                                    $id=$_GET['id'];
                                    $query=mysqli_query($conn,"SELECT * FROM user_offline WHERE id='$id'");
                                    while($row=mysqli_fetch_array($query)){
                                    ?>
                                    <tr>
                                        <td>ID</td>
                                        <td> : </td>
                                        <td><?php echo $row['id'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama</td>
                                        <td> : </td>
                                        <td><?php echo $row['nama'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>NIK</td>
                                        <td> : </td>
                                        <td><?php echo $row['nik'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td> : </td>
                                        <td><?php echo $row['email'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td> : </td>
                                        <td><?php echo $row['alamat'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal lahir</td>
                                        <td> : </td>
                                        <td><?php echo $row['tempat_lahir'] ?>, <?php echo $row['tanggal_lahir'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Jenis kelamin</td>
                                        <td> : </td>
                                        <td><?php echo $row['jenis_kelamin'] ?></td>
                                    </tr>
                                </table>
                                <?php } ?>
                            </div>
                            <div class="card-footer">
                                <a href="data_pelanggan.php" class="btn btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
                </main>
<?php
    include('../include/footer.html');
?>