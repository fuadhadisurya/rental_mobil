<?php
include('../include/headeradmin.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include '../include/connect.php';

if(isset($_POST['submit'])) {
    $id=$_POST['id'];
    $nama=$_POST['nama'];
    $nopol=$_POST['no_polisi'];
    $jenis=$_POST['jenis'];
    $transmisi=$_POST['transmisi'];
    $harga = $_POST['harga'];
    $status = $_POST['status'];
    $foto_tipe =strtolower($_FILES['gambar']['type']);
    $foto_name =$_FILES['gambar']['name'];
    $foto_size =$_FILES['gambar']['size'];
    $foto_temp =$_FILES['gambar']['tmp_name'];
    $foto_error =$_FILES['gambar']['error'];
    
    //validasi harga Numeric
    if($harga!=is_numeric($harga)){
    echo '<script>
      alert("Harga harus berupa angka");
      window.location.href ="edit_mobil.php";
    </script>';
    die();
    }

//hapus gambar lama
if($foto_name==true){
    $result=mysqli_query($conn,"SELECT foto FROM mobil WHERE id='$id'");
    $row=mysqli_fetch_assoc($result);
    unlink("../image/mobil/".$row['foto']);
}

move_uploaded_file($foto_temp,'../image/mobil/'.$foto_name);
    if($foto_name==""){
        $query="UPDATE mobil SET nama='$nama',transmisi='$transmisi',jenis='$jenis',no_polisi='$nopol',harga_sewa='$harga',status='$status' WHERE id='$id'";
    }else{
        $query="UPDATE mobil SET nama='$nama',transmisi='$transmisi',jenis='$jenis',no_polisi='$nopol',harga_sewa='$harga',status='$status',foto='$foto_name' WHERE id='$id'";
    }

    $result=mysqli_query($conn, $query) or die (mysqli_error($conn));
    if( $result ) {
        // kalau berhasil alihkan ke halaman list-siswa.php
        echo '<script> alert ("Data mobil berhasil diganti");</script>';
        echo '<script>window.location.assign("../admin/data_mobil.php")</script>';
    } else {
        // kalau gagal tampilkan pesan
        echo '<script> alert ("Data mobil gagal diperbarui");</script>';
        echo '<script>window.location.assign("edit_mobil.php")</script>';
    }
}
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Edit mobil</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="data_mobil.php">Data mobil</a></li>
                            <li class="breadcrumb-item active">Edit mobil</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                            <?php
                            //untuk menampilkan data sebelum di update/diedit
                            $id = $_GET['id'];
                            $result = mysqli_query($conn, "SELECT * FROM mobil WHERE id = '$id'");
                            $mobil = mysqli_fetch_array($result);
                            ?>
                            <form method="post" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?php echo $mobil['id'] ?>">
                            <div class="form-row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputNama">Nama Mobil</label>
                                            <input class="form-control py-4" name="nama" id="inputNama" type="text" placeholder="Masukkan nama. Format: 'Merk Tipe'" value="<?php echo $mobil['nama'] ?>" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputNopol">No. Polisi</label>
                                            <input class="form-control py-4" name="no_polisi" id="inputNopol" type="text" placeholder="Masukkan No. polisi" value="<?php echo $mobil['no_polisi'] ?>" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputJenis">Jenis</label>
                                                <select class="form-control" id="jenis" name="jenis" required="required" autofocus="autofocus">
                                                    <option selected><?php echo $mobil['jenis'] ?></option>
                                                    <option>MVP</option>
                                                    <option>Pick-up</option>
                                                    <option>SUV</option>
                                                    <option>Sedan</option>
                                                    <option>City car</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputTransmisi">Transmisi</label>
                                                <select class="form-control" id="inputTransmisi" name="transmisi" required="required" autofocus="autofocus">
                                                    <option selected><?php echo $mobil['transmisi'] ?></option>
                                                    <option>A/T</option>
                                                    <option>M/T</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputHarga">Harga</label>
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">Rp. </div>
                                                </div>
                                                <input class="form-control py-4" name="harga" id="inputHarga" type="text" placeholder="Masukkan Harga" value="<?php echo $mobil['harga_sewa'] ?>" required="required" autofocus="autofocus" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                        <label class="small mb-1" for="inputGambar">Upload gambar</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="gambar" name="gambar" >
                                                <label class="custom-file-label" for="customFile"><?php echo $mobil['foto'] ?></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="Tersedia" <?php if($mobil['status']=="Tersedia"){echo "checked";}?> required="required" autofocus="autofocus">
                                        <label class="form-check-label" for="inlineRadio1">Tersedia</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="Tidak tersedia" <?php if($mobil['status']=="Tidak tersedia"){echo "checked";}?> required="required" autofocus="autofocus">
                                        <label class="form-check-label" for="inlineRadio2">Tidak tersedia</label>
                                    </div>
                                </div>
                                <div class="form-group mt-4 mb-0">
                                    <button class="btn btn-primary btn-block" type="submit" name="submit">Update</button>
                                    <a href="../admin/data_mobil.php" class="btn btn-secondary btn-block">batalkan</a>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </main>
                <script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
<?php
    include('../include/footer.html');
?>