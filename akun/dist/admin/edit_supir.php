<?php
include('../include/headeradmin.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include '../include/connect.php';

if(isset($_POST['submit'])) {
    $id=$_POST['id'];
    $nama=$_POST['nama'];
    $nik=$_POST['nik'];
    $no_hp=$_POST['no_hp'];
    $email=$_POST['email'];
    $alamat = $_POST['alamat'];
    $status = $_POST['status'];

    $query="UPDATE supir SET nama='$nama',no_hp='$no_hp',alamat='$alamat',email='$email',status='$status' WHERE id='$id'";
    $result=mysqli_query($conn, $query) or die (mysqli_error($conn));
    if( $result ) {
        // kalau berhasil alihkan ke halaman list-siswa.php
        echo '<script> alert ("Profil Berhasil diganti");</script>';
        echo '<script>window.location.assign("../admin/data_supir.php")</script>';
    } else {
        // kalau gagal tampilkan pesan
        echo '<script> alert ("Profil gagal diperbarui");</script>';
        echo '<script>window.location.assign("edit_supir.php")</script>';
    }
    
}
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Ubah Profil</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="../admin/index.php">Dashboard</a></li>
                            <li class="breadcrumb-item active">Ubah Profil</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                            <?php
                            //untuk menampilkan data sebelum di update/diedit
                            $id = $_GET['id'];
                            $result = mysqli_query($conn, "SELECT * FROM supir WHERE id = '$id'");
                            $user = mysqli_fetch_array($result);
                            ?>
                            <form method="post">
                                <input type="hidden" name="id" value="<?php echo $user['id'] ?>">
                                <div class="form-group">
                                    <label class="small mb-1" for="inputFullName">Name Lengkap</label>
                                    <input class="form-control py-4" name="nama" id="inputFullName" type="text" placeholder="Masukkan nama lengkap" value="<?php echo $user['nama'] ?>" required="required" autofocus="autofocus" />
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputNIK">NIK</label>
                                            <input class="form-control py-4" name="nik" id="inputNIK" type="text" placeholder="Masukkan NIK" value="<?php echo $user['nik'] ?>" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputnohp">No. HP</label>
                                            <input class="form-control py-4" name="no_hp" id="inputnohp" type="text" placeholder="Masukkan No. HP" value="<?php echo $user['no_hp'] ?>" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputemail">email</label>
                                            <input class="form-control py-4" name="email" id="inputemail" type="email" placeholder="Masukkan No. HP" value="<?php echo $user['email'] ?>" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="small mb-1" for="inputAlamat">Alamat</label>
                                    <textarea class="form-control" name="alamat" id="inputAlamat" rows="2" placeholder="Masukkan alamat" required="required" autofocus="autofocus"><?php echo $user['alamat'] ?></textarea>
                                </div>
                                <label class="small mb-1" for="inputStatus">Status</label>
                                <div class="form-row">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="Tersedia" <?php if($user['status']=="Tersedia"){echo "checked";}?> required="required" autofocus="autofocus">
                                        <label class="form-check-label" for="inlineRadio1">Tersedia</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="Tidak tersedia" <?php if($user['status']=="Tidak tersedia"){echo "checked";}?> required="required" autofocus="autofocus">
                                        <label class="form-check-label" for="inlineRadio2">Tidak tersedia</label>
                                    </div>
                                </div>
                                <div class="form-group mt-4 mb-0">
                                    <button class="btn btn-primary btn-block" type="submit" name="submit">Ubah</button>
                                    <a href="../admin/data_supir.php" class="btn btn-secondary btn-block">batalkan</a>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </main>
<?php
    include('../include/footer.html');
?>