<?php
include('../include/headeradmin.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include '../include/connect.php';

if(isset($_POST['submit'])) {
    $id_pelanggan=$_POST['id_pelanggan'];
    $id_mobil=$_POST['id_mobil'];
    $tgl_sewa=$_POST['tgl_sewa'];
    $jatuh_tempo=$_POST['jatuh_tempo'];
    $supir = $_POST['supir'];
    $jaminan=$_POST['jaminan'];
    $status = $_POST['status'];
    $status_pembayaran=$_POST['status_pembayaran'];
    $harga=$_POST['harga'];

    $tgl1 = new DateTime($_POST['tgl_sewa']);
    $tgl2 = new DateTime($_POST['jatuh_tempo']);
    $lama = $tgl2->diff($tgl1)->days + 1;
    
    if($supir=="Ya"){
        $jasa_supir=100000;
        $total=($harga+$jasa_supir)*$lama;
    }else{
        $total=$harga*$lama;
    }
    $query_mobil="UPDATE mobil SET status='$status' WHERE id='$id_mobil'";
    $result_mobil=mysqli_query($conn, $query_mobil) or die (mysqli_error($conn));

    $query_transaksi="INSERT INTO transaksi (id,id_user_offline,id_mobil,supir,tgl_sewa,jatuh_tempo,lama,status_pembayaran,jaminan,total_pembayaran,tanggal_pesan) 
    VALUES (null,'$id_pelanggan','$id_mobil','$supir','$tgl_sewa','$jatuh_tempo','$lama','$status_pembayaran','$jaminan','$total',NOW())";
    $result_transaksi=mysqli_query($conn, $query_transaksi) or die (mysqli_error($conn));
    if( $result_transaksi && $result_mobil) {
        echo '<script> alert ("Berhasil");</script>';
        echo "<script>window.location.assign('konfirmasi_offline.php')</script>";
    } else {
        echo '<script> alert ("Gagal, coba ulangi lagi");</script>';
        echo '<script>window.location.assign("sewa.php")</script>';
    }
    var_dump($query_transaksi);
}
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Form sewa (OTS)</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                            <li class="breadcrumb-item active">Form sewa (OTS)</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                            <?php
                                include('../include/connect.php');
                                //untuk menampilkan data sebelum di update/diedit
                                $id = $_GET['id'];
                                $result = mysqli_query($conn, "SELECT * FROM mobil WHERE id = '$id'");
                                $mobil = mysqli_fetch_array($result);
                                $harga=$mobil['harga_sewa'];
                            ?>
                            <form method="post">
                                <div class="form-row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputID">ID pelanggan</label>
                                            <input class="form-control py-4" onkeyup="pelanggan()" name="id_pelanggan" id="id_pelanggan" type="text" placeholder="Masukkan ID" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputNama">Nama</label>
                                            <input class="form-control py-4" name="nama_pelanggan" id="nama_pelanggan" type="text" required="required" autofocus="autofocus" disabled/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputNIK">NIK</label>
                                            <input class="form-control py-4" name="nik" id="nik" type="text" required="required" autofocus="autofocus" disabled />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputnohp">No. HP</label>
                                            <input class="form-control py-4" name="no_hp" id="no_hp" type="text" required="required" autofocus="autofocus" disabled/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="small mb-1" for="inputAlamat">Alamat</label>
                                    <textarea class="form-control" name="alamat" id="alamat" rows="2" required="required" autofocus="autofocus" disabled></textarea>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputTempatLahir">Tempat lahir</label>
                                            <input class="form-control" name="tempat_lahir" id="tempat_lahir" type="text" required="required" autofocus="autofocus" disabled/>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputTanggalLahir">Tanggal lahir</label>
                                            <input class="form-control" name="tanggal_lahir" id="tanggal_lahir" type="date" required="required" autofocus="autofocus" disabled />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputJenisKelamin">Jenis kelamin</label>
                                            <input class="form-control" name="jenis_kelamin" id="gender" type="text" required="required" autofocus="autofocus" disabled/>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="id_mobil" value="<?php echo $mobil['id'] ?>">
                                <div class="form-row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputNama">Nama Mobil</label>
                                            <input class="form-control py-4" name="nama" id="inputNama" type="text" value="<?php echo $mobil['nama'] ?>" required="required" autofocus="autofocus" disabled />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputNopol">No. Polisi</label>
                                            <input class="form-control py-4" name="no_polisi" id="inputNopol" type="text" value="<?php echo $mobil['no_polisi'] ?>" required="required" autofocus="autofocus" disabled/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputJenis">Jenis</label>
                                            <input class="form-control" name="jenis" id="inputJenis" type="text" value="<?php echo $mobil['jenis'] ?>" required="required" autofocus="autofocus" disabled/>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputTransmisi">Transmisi</label>
                                            <input class="form-control" name="transmisi" id="inputTransmisi" type="text" value="<?php echo $mobil['transmisi'] ?>" required="required" autofocus="autofocus" disabled/>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputHarga">Harga sewa</label>
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">Rp. </div>
                                                </div>
                                                <input class="form-control" name="harga_curr" id="inputHarga" type="text" value="<?php echo number_format($harga, 0, ",",","); echo "/Hari" ?>" required="required" autofocus="autofocus" disabled/>
                                                <input type="hidden" value="<?php echo $mobil['harga_sewa'] ?>" name="harga">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputSewa">Tanggal ambil</label>
                                            <input class="form-control" name="tgl_sewa" id="inputSewa" type="date" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputTempo">Tanggal kembali</label>
                                            <input class="form-control" name="jatuh_tempo" id="inputTempo" type="date" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="small mb-1" for="inputSupir">Dengan supir?</label>
                                    <select class="form-control" id="inputSupir" name="supir" required="required" autofocus="autofocus">
                                        <option>Ya</option>
                                        <option>Tidak</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="small mb-1" for="inputJaminan">Jaminan</label>
                                    <select class="form-control" id="inputJaminan" name="jaminan" required="required" autofocus="autofocus">
                                        <option>STNK</option>
                                        <option>Sertifikat rumah</option>
                                    </select>
                                </div>
                                <input type="hidden" value="Tidak tersedia" name="status">
                                <input type="hidden" value="Disewakan" name="status_pembayaran">
                                <div class="form-group mt-4 mb-0">
                                    <button class="btn btn-primary btn-block" type="submit" name="submit">Sewa</button>
                                    <a href="../admin/pilih_mobil.php" class="btn btn-secondary btn-block">Batalkan</a>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </main>
<?php
    include('../include/footer.html');
?>
<script type="text/javascript">
    function pelanggan(){
        var id = $("#id_pelanggan").val();
			$.ajax({
                url: 'ajax_pelanggan.php',
                data:'id='+id ,
            }).done(function(data){
                var json = data,
                obj = JSON.parse(json);
                $('#nama_pelanggan').val(obj.nama_pelanggan);
                $('#nik').val(obj.nik);
                $('#no_hp').val(obj.no_hp);
                $('#alamat').val(obj.alamat);
                $('#tempat_lahir').val(obj.tempat_lahir);
                $('#tanggal_lahir').val(obj.tanggal_lahir);
                $('#gender').val(obj.gender);
            });
    }
</script>
<script type="text/javascript">
    function mobil(){
        var id = $("#id_mobil").val();
			$.ajax({
                url: 'ajax_mobil.php',
                data:'id='+id ,
            }).done(function(data){
                var json = data,
                obj = JSON.parse(json);
                $('#nama_mobil').val(obj.nama_mobil);
                $('#no_polisi').val(obj.no_polisi);
                $('#jenis').val(obj.jenis);
                $('#transmisi').val(obj.transmisi);
                $('#harga_sewa').val(obj.harga_sewa);
            });
    }
</script>
