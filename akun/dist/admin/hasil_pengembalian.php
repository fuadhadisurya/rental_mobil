<?php
include('../include/headeradmin.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include '../include/connect.php';

?>

            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Info Denda</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="data_peminjaman.php">Data Peminjaman</a></li>
                            <li class="breadcrumb-item active">Info Denda</li>
                        </ol>

                        <div class="card mb-4">
                            <div class="card-header">
                                Info Denda
                            </div>
                            <div class="card-body">
                                <table>
                                    <?php
                                    include('../include/connect.php');
                                    $id=$_GET['id'];
                                    $query=mysqli_query($conn,"SELECT *, mobil.nama AS nama_mobil, user.nama AS nama_user, transaksi.id AS id_pesanan, user_offline.nama AS nama_pelanggan, transaksi.foto AS bukti_transaksi, transaksi.id AS id_transaksi,
                                    user.nik AS nik_online, user_offline.nik AS nik_offline, user.alamat AS alamat_online, user_offline.alamat AS alamat_offline, user.no_hp AS no_hp_online, user_offline.no_hp AS no_hp_offline, user.email AS email_online, user_offline.email AS email_offline, user.jenis_kelamin AS jenis_kelamin_online, user_offline.jenis_kelamin AS jenis_kelamin_offline
                                    FROM transaksi  INNER JOIN mobil ON transaksi.id_mobil=mobil.id LEFT JOIN user ON transaksi.id_user=user.id LEFT JOIN user_offline ON transaksi.id_user_offline=user_offline.id WHERE transaksi.id='$id'");
                                    while($row=mysqli_fetch_array($query)){
                                        $harga=$row['harga_sewa'];
                                        $total=$row['total_pembayaran'];
                                        $denda=$row['denda'];
                                    ?>
                                    <?php if($row['nama_user']==true){?>
                                    <tr>
                                        <td>Nama</td>
                                        <td> : </td>
                                        <td><?php echo $row['nama_user'] ?></td>
                                    </tr>
                                    <?php } else { ?>
                                    <tr>
                                        <td>Nama</td>
                                        <td> : </td>
                                        <td><?php echo $row['nama_pelanggan'] ?></td>
                                    </tr>
                                    <?php } ?>
                                    <?php if($row['nama_user']==true){?>
                                    <tr>
                                        <td>NIK</td>
                                        <td> : </td>
                                        <td><?php echo $row['nik_online'] ?></td>
                                    </tr>
                                    <?php } else { ?>
                                    <tr>
                                        <td>NIK</td>
                                        <td> : </td>
                                        <td><?php echo $row['nik_offline'] ?></td>
                                    </tr>
                                    <?php } ?>
                                    <?php if($row['nama_user']==true){?>
                                    <tr>
                                        <td>Jenis Kelamin</td>
                                        <td> : </td>
                                        <td><?php echo $row['jenis_kelamin_online'] ?></td>
                                    </tr>
                                    <?php } else { ?>
                                    <tr>
                                        <td>Jenis Kelamin</td>
                                        <td> : </td>
                                        <td><?php echo $row['jenis_kelamin_offline'] ?></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td>Nama mobil</td>
                                        <td> : </td>
                                        <td><?php echo $row['nama_mobil'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>No. Polisi</td>
                                        <td> : </td>
                                        <td><?php echo $row['no_polisi'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal ambil</td>
                                        <td> : </td>
                                        <td><?php echo $row['tgl_sewa'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Jatuh tempo</td>
                                        <td> : </td>
                                        <td><?php echo $row['jatuh_tempo'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal kembali</td>
                                        <td> : </td>
                                        <td><?php echo $row['tgl_kembali'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Lama sewa</td>
                                        <td> : </td>
                                        <td><?php echo $row['lama'] ?> Hari</td>
                                    </tr>
                                    <tr>
                                        <td>Jaminan</td>
                                        <td> : </td>
                                        <td><?php echo $row['jaminan'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Denda</td>
                                        <td> : </td>
                                        <td>Rp. <?php echo number_format($denda, 0, ",",","); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Keterangan denda</td>
                                        <td> : </td>
                                        <td><?php echo $row['alasan'] ?></td>
                                    </tr>
                                </table>
                                <?php } ?>
                            </div>
                            <div class="card-footer">
                                <a href="data_peminjaman.php" class="btn btn-primary">Tutup</a>
                            </div>
                        </div>
                    </div>
                </main>

<?php
    include('../include/footer.html');
?>