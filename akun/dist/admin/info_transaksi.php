<?php
include('../include/headeruser.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Konfirmasi sewa</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="kendaraan.php">Sewa mobil</a></li>
                            <li class="breadcrumb-item active">Konfirmasi sewa</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-header text-muted">
                                <i class="fas fa-user"></i> Profil user
                            </div>
                            <div class="card-body">
                                ce
                            </div>
                        </div>
                        <div class="card mb-4">
                            <div class="card-header text-muted">
                                <i class="fas fa-car"></i> Profil mobil
                            </div>
                            <div class="card-body">
                                <?php
                                include('../include/connect.php');
                                //untuk menampilkan data sebelum di update/diedit
                                $id = $_GET['id'];
                                $result = mysqli_query($conn, "SELECT * FROM mobil WHERE id = '$id'");
                                $mobil = mysqli_fetch_array($result);
                                ?>
                                <form method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="<?php echo $mobil['id'] ?>">
                                <div class="form-row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputNama">Nama Mobil</label>
                                            <input class="form-control py-4" name="nama" id="inputNama" type="text" placeholder="Masukkan nama. Format: 'Merk Tipe'" value="<?php echo $mobil['nama'] ?>" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputNopol">No. Polisi</label>
                                            <input class="form-control py-4" name="no_polisi" id="inputNopol" type="text" placeholder="Masukkan No. polisi" value="<?php echo $mobil['no_polisi'] ?>" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputJenis">Jenis</label>
                                                <select class="form-control" id="jenis" name="jenis" required="required" autofocus="autofocus">
                                                    <option selected><?php echo $mobil['jenis'] ?></option>
                                                    <option>MVP</option>
                                                    <option>Pick-up</option>
                                                    <option>SUV</option>
                                                    <option>Sedan</option>
                                                    <option>City car</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputTransmisi">Transmisi</label>
                                                <select class="form-control" id="inputTransmisi" name="transmisi" required="required" autofocus="autofocus">
                                                    <option selected><?php echo $mobil['transmisi'] ?></option>
                                                    <option>A/T</option>
                                                    <option>M/T</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputHarga">Harga</label>
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">Rp. </div>
                                                </div>
                                                <input class="form-control py-4" name="harga" id="inputHarga" type="text" placeholder="Masukkan Harga" value="<?php echo $mobil['harga_sewa'] ?>" required="required" autofocus="autofocus" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mt-4 mb-0">
                                    <button class="btn btn-primary btn-block" type="submit" name="submit">Sewa</button>
                                    <a href="../admin/data_mobil.php" class="btn btn-secondary btn-block">Batalkan</a>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </main>
<?php
    include('../include/footer.html');
?>