<?php
include('../include/headeradmin.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include '../include/connect.php';

if(isset($_POST['submit'])) {
    $id=$_POST['id'];
    $alasan=$_POST['alasan'];

    $query="UPDATE transaksi SET alasan='$alasan' WHERE id=$id";
    $result=mysqli_query($conn, $query) or die (mysqli_error($conn));
    if( $result) {
        // kalau berhasil alihkan ke halaman hasil_pengembalian.php
        echo "<script> alert('Berhasil! data telah disimpan');</script>";
        echo "<script>window.location.assign('hasil_pengembalian.php?id=$id')</script>";
    } else {
        // kalau gagal tampilkan pesan
        echo "<script> alert('gagal diperbarui');</script>";
        echo "<script>window.location.assign('keterangan_denda.php?id=$id')</script>";
    }
}
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <div class="card mb-4 mt-4">
                            <div class="card-header">
                                Keterangan Denda
                            </div>
                            <div class="card-body">
                            <?php
                            //untuk menampilkan data sebelum di update/diedit
                            $id = $_GET['id'];
                            $result = mysqli_query($conn, "SELECT * FROM transaksi WHERE id = '$id'");
                            $user = mysqli_fetch_array($result);
                            ?>
                            <form method="post">
                            <input type="hidden" name="id" value="<?php echo $user['id'] ?>">
                                <div class="form-row">
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <textarea class="form-control" id="alasan" name="alasan" placeholder="Masukkan Keterangan Denda" rows="3" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label for=""></label>
                                        <button class="btn btn-primary btn-block" type="submit" name="submit">Selesai</button>
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </main>
<?php
    include('../include/footer.html');
?>