<?php
include('../include/headeradmin.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include '../include/connect.php';

$result = mysqli_query($conn, "SELECT * FROM transaksi ORDER BY id DESC LIMIT 1");
$data = mysqli_fetch_array($result);
if($data['supir']=='Ya'){
	if(isset($_POST['submit'])) {
		$id= $_POST['id'];
		$id_supir=$_POST['id_supir'];
		$status_supir=$_POST['status_supir'];

		$query_supir="UPDATE supir SET status='$status_supir' WHERE id='$id_supir'";
		$result_supir=mysqli_query($conn, $query_supir) or die (mysqli_error($conn));

		$query="UPDATE transaksi SET id_supir='$id_supir' WHERE id='$id'";
		$result=mysqli_query($conn, $query) or die (mysqli_error($conn));
		if( $result && $result_supir) {
			echo '<script> alert ("Berhasil!");</script>';
			echo '<script>window.location.assign("data_peminjaman.php")</script>';
		} else {
			echo '<script> alert ("Gagal input");</script>';
			echo '<script>window.location.assign("konfirmasi_offline.php")</script>';
		}
	}
?>
			<div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Konfirmasi transaksi</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="../admin/index.php">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="../admin/konfirmasi_pembayaran.php">Konfirmasi pembayaran</a></li>
                            <li class="breadcrumb-item active">Konfirmasi transaksi</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                            <?php
                                include('../include/connect.php');
                                $query=mysqli_query($conn,"SELECT * FROM transaksi ORDER BY id DESC LIMIT 1");
                                while($row=mysqli_fetch_array($query)){
                            ?>
                            <form method="post">
								<input type="hidden" value="Tidak tersedia" name="status_supir">
                                <input type="hidden" name="id" value="<?php echo $row['id'] ?>">
                                <div class="form-group">
                                    <label class="small mb-1" for="inputSupir">Input ID supir</label>
									<?php
										$query=mysqli_query($conn,"SELECT * FROM supir WHERE status='Tersedia' ORDER BY terakhir_kerja ASC");?>
										<select class="form-control" id="inputSupir" name="id_supir" required="required" autofocus="autofocus">
											<? while($row=mysqli_fetch_array($query)){ ?>
										    <option><?php echo $row['id']?></option>
											<?php }	?>
										</select>
                                </div>
                                <div class="form-group mt-4 mb-0">
                                    <button class="btn btn-primary btn-block" type="submit" name="submit">Kirim</button>
                                    <a href="../admin/data_peminjaman.php" class="btn btn-secondary btn-block">Kembali</a>
                                </div>
                            </form>
                            <?php } ?>
                            </div>
                        </div>
					</div>
                </main>
<?php
include('../include/footer.html');
}
else{
	require '../include/connect.php';

	$id= $_GET['id'];
	$status_pembayaran="Disewakan";
	mysqli_query($conn, "UPDATE transaksi SET status_pembayaran='$status_pembayaran' WHERE id='$id'");
	echo "
	<script>
		document.location.href='data_peminjaman.php';
	</script>
	";
}
?>