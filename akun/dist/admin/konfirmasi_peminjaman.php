<?php
include('../include/headeradmin.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include('../include/pembatalan_otomatis.php');
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Konfirmasi pembayaran</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item active">Konfirmasi pembayaran</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table"></i> Konfirmasi pembayaran
                            </div>
                            <div class="card-body">
                            <div class="table-responsive">
                                    <table class="table table-bordered" id="mobil" width="100%" cellspacing="0">
                                        <thead>
                                            <tr class="text-center">
                                                <th>ID pesanan</th>
                                                <th>Nama pelanggan</th>
                                                <th>Nama mobil</th>
                                                <th>No polisi</th>
                                                <th>Tanggal ambil</th>
                                                <th>Jatuh tempo</th>
                                                <th>Total pembayaran</th>
                                                <th>Tanggal pemesanan</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                                include('../include/connect.php');
                                                $query=mysqli_query($conn,"SELECT *, mobil.nama AS nama_mobil, user.nama AS nama_user, transaksi.id AS id_pesanan
                                                FROM transaksi  INNER JOIN mobil ON transaksi.id_mobil=mobil.id INNER JOIN user ON transaksi.id_user=user.id WHERE status_pembayaran='Menunggu konfirmasi'");
                                                while($row=mysqli_fetch_array($query)){
                                                    $harga=$row['total_pembayaran'];
                                            ?>
                                                        <tr>
                                                            <td><?php echo $row['id_pesanan'] ?></td>
                                                            <td><?php echo $row['nama_user'] ?></td>
                                                            <td><?php echo $row['nama_mobil'] ?></td>
                                                            <td><?php echo $row['no_polisi'] ?></td>
                                                            <td><?php echo $row['tgl_sewa'] ?></td>
                                                            <td><?php echo $row['jatuh_tempo'] ?></td>
                                                            <td>Rp. <?php echo number_format($harga, 0, ",",",") ?></td>
                                                            <td><?php echo $row['tanggal_pesan'] ?></td>
                                                            <td>
                                                                <a href="detail_transaksi.php?id=<?php echo $row["id_pesanan"]?>"class="btn btn-primary"><i class="fas fa-info-circle"></i></a>
                                                                <a onclick="return confirm('Apakah kamu yakin untuk mengkonfirmasi transaksi?')" href="konfirmasi.php?id=<?php echo $row["id_pesanan"]?>" class="btn btn-success"><i class="fas fa-check-circle"></i></a>
                                                                <a onclick="return confirm('Apakah kamu yakin untuk menolak transaksi?')" href="alasan.php?id=<?php echo $row["id_pesanan"]?>" class="btn btn-danger"><i class="fas fa-times-circle"></i></a>
                                                            </td>
                                                        </tr>
                                            <?php
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <script>
                $(document).ready(function() {
                    $('#mobil').DataTable();
                } );
                </script>
<?php
    include('../include/footer.html');
?>