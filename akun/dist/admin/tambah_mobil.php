<?php
include('../include/headeradmin.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include '../include/connect.php';

if(isset($_POST['submit'])) {
    $nama=$_POST['nama'];
    $nopol=$_POST['no_polisi'];
    $jenis=$_POST['jenis'];
    $transmisi=$_POST['transmisi'];
    $harga = $_POST['harga'];
    $status=$_POST['status'];
    $foto_tipe =strtolower($_FILES['gambar']['type']);
    $foto_name =$_FILES['gambar']['name'];
    $foto_size =$_FILES['gambar']['size'];
    $foto_temp =$_FILES['gambar']['tmp_name'];
    $foto_error =$_FILES['gambar']['error'];

    //upload foto
//jika tidak ada foto yang diupload
if($foto_error ===4){
    echo '<script>
    alert("GAGAL! Tidak ada Gambar yang diupload");
    window.location.href ="tambah_mobil.php";
  </script>';
  die();
}
// check type file yang akan diupload
//JAngan Lupa Check Juga di php.ini mengenai max upload

if(
  $foto_tipe != "image/jpg" AND
  $foto_tipe != "image/jpeg" AND
  $foto_tipe != "image/pjpeg" AND
  $foto_tipe != "image/png" 
){
   echo '<script>
    alert("GAGAL! Tipe file yang diperbolehkan jpg, jpeg, pjpeg, png");
    window.location.href ="tambah_mobil.php";
  </script>';
  die();
}

//check jika ukuran file terlalu besar
  if($foto_size > 1000000){
     echo '<script>
    alert("GAGAL! Ukuran terlalu besar");
    window.location.href ="tambah_mobil.php";
  </script>';  
  die();
  }

//Cek gambar
$result=mysqli_query($conn,"SELECT foto FROM mobil WHERE foto='$foto_name'");
if(mysqli_fetch_assoc($result)){
  echo '<script>
    alert("Nama File Sudah ada");
    window.location.href ="tambah_mobil.php";
  </script>';
  die();
}

move_uploaded_file($foto_temp,'../image/mobil/'.$foto_name);

    //validasi harga Numeric
if($harga!=is_numeric($harga)){
    echo '<script>
      alert("Harga harus berupa angka");
      window.location.href ="tambah_mobil.php";
    </script>';
    die();
  }
  //check apakah ada Nopol yang sama???
  $result=mysqli_query($conn,"SELECT no_polisi FROM mobil WHERE no_polisi='$nopol'");
  if(mysqli_fetch_assoc($result)){
    echo '<script>
      alert("Nomor Polisi Sudah Ada");
      window.location.href ="tambah_mobil.php";
    </script>';
    die();
  }

    $query="INSERT INTO mobil (id,nama,transmisi,jenis,no_polisi,harga_sewa,status,foto) VALUES (null,'$nama','$transmisi','$jenis','$nopol','$harga','$status','$foto_name')";
    $result=mysqli_query($conn, $query) or die (mysqli_error($conn));

    if(mysqli_affected_rows($conn) > 0){
      echo '<script>
         alert("Berhasil ditambahkan")
         window.location.href ="data_mobil.php";
      </script>';
      }else{
            echo '<script>
                alert("Gagal ditambahkan")
                window.location.href ="tambah_mobil.php";
            </script>';
    }    
}
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Tambah mobil</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="data_mobil.php">Data mobil</a></li>
                            <li class="breadcrumb-item active">Tambah mobil</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                            <form method="post" enctype="multipart/form-data">
                            <div class="form-row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputNama">Nama Mobil</label>
                                            <input class="form-control py-4" name="nama" id="inputNama" type="text" placeholder="Masukkan nama. Format: 'Merk Tipe'" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputNopol">No. Polisi</label>
                                            <input class="form-control py-4" name="no_polisi" id="inputNopol" type="text" placeholder="Masukkan No. polisi" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputJenis">Jenis</label>
                                                <select class="form-control" id="jenis" name="jenis" required="required" autofocus="autofocus">
                                                    <option selected>Pilih jenis</option>
                                                    <option>MVP</option>
                                                    <option>Pick-up</option>
                                                    <option>SUV</option>
                                                    <option>Sedan</option>
                                                    <option>City car</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputTransmisi">Transmisi</label>
                                                <select class="form-control" id="inputTransmisi" name="transmisi" required="required" autofocus="autofocus">
                                                    <option selected>Pilih transmisi</option>
                                                    <option>A/T</option>
                                                    <option>M/T</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputHarga">Harga</label>
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">Rp. </div>
                                                </div>
                                                <input class="form-control py-4" name="harga" id="inputHarga" type="text" placeholder="Masukkan Harga" required="required" autofocus="autofocus" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                        <label class="small mb-1" for="inputGambar">Upload gambar</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="gambar" name="gambar" required="required" autofocus="autofocus">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="status" value="Tersedia">
                                <div class="form-group mt-4 mb-0">
                                    <button class="btn btn-primary btn-block" type="submit" name="submit">Tambah</button>
                                    <a href="../admin/data_mobil.php" class="btn btn-secondary btn-block">batalkan</a>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </main>
                <script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
<?php
    include('../include/footer.html');
?>