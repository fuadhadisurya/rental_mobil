<?php
include('../include/headeradmin.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include '../include/connect.php';

if(isset($_POST['submit'])){
    $nama=$_POST['nama'];
    $nik=$_POST['nik'];
    $kontak=$_POST['kontak'];
    $alamat=$_POST['alamat'];
    $tl=$_POST['tl']; //tempat lahir
    $tgl=$_POST['tgl']; //tanggal lahir
    $jk=$_POST['jk']; //jenis Kelamin
    $email=$_POST['email'];

//validasi NIK dan nohp Numeric
if($nik!=is_numeric($nik) && $kontak!=is_numeric($kontak)){
    echo '<script>
      alert("NIK/No. Hp harus berupa angka");
      window.location.href ="tambah_pelanggan.php";
    </script>';
    die();
  }
//check apakah ada NIK yang sama???
  $result=mysqli_query($conn,"SELECT nik FROM user_offline WHERE nik='$nik'");
  if(mysqli_fetch_assoc($result)){
    echo '<script>
      alert("Nomor Induk Kependudukan Sudah Terdaftar");
      window.location.href ="tambah_pelanggan.php";
    </script>';
    die();
  }
//check apakah ada email yang sama???
$result=mysqli_query($conn,"SELECT email FROM user_offline WHERE email='$email'");
if(mysqli_fetch_assoc($result)){
  echo '<script>
    alert("Email Sudah Terdaftar");
    window.location.href ="tambah_pelanggan.php";
  </script>';
  die();
}
  //Query Insert data
  $query="INSERT INTO user_offline (id,nama,email,nik,no_hp,alamat,tempat_lahir,tanggal_lahir,jenis_kelamin) 
  VALUES (null,'$nama','$email','$nik','$kontak','$alamat','$tl','$tgl','$jk')";
  mysqli_query($conn,$query);
  if(mysqli_affected_rows($conn) > 0)
  {
    echo '<script>
       alert("Berhasil ditambahkan")
       window.location.href ="data_pelanggan.php";
    </script>';
    }else{
    echo '<script>
        alert("Gagal")
        window.location.href ="tambah_pelanggan.php";
     </script>';
  }

}//milik submit
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Tambah pelanggan</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="data_supir.php">Data pelanggan</a></li>
                            <li class="breadcrumb-item active">Tambah pelanggan</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                            <form method="post">
                                <div class="form-group">
                                    <label class="small mb-1" for="inputFullName">Name Lengkap</label>
                                    <input class="form-control py-4" name="nama" id="inputFullName" type="text" placeholder="Masukkan nama lengkap" required="required" autofocus="autofocus" />
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputNIK">NIK</label>
                                            <input class="form-control py-4" name="nik" id="inputNIK" type="text" placeholder="Masukkan NIK" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputnohp">No. HP</label>
                                            <input class="form-control py-4" name="kontak" id="inputnohp" type="text" placeholder="Masukkan No. HP" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="small mb-1" for="inputAlamat">Alamat</label>
                                    <textarea class="form-control" name="alamat" id="inputAlamat" rows="2" placeholder="Masukkan alamat" required="required" autofocus="autofocus"></textarea>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputTempatLahir">Tempat lahir</label>
                                            <input class="form-control" name="tl" id="inputTempatLahir" type="text" placeholder="Masukkan tempat lahir" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputTanggalLahir">Tanggal lahir</label>
                                            <input class="form-control" name="tgl" id="inputTanggalLahir" type="date" placeholder="Masukkan tanggal lahir" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                </div>
                                <label class="small mb-1" for="inputJenisKelamin">Jenis Kelamin</label>
                                <div class="form-row">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="jk" id="inlineRadio1" value="Laki-laki" required="required" autofocus="autofocus">
                                        <label class="form-check-label" for="inlineRadio1">Laki-laki</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="jk" id="inlineRadio2" value="Perempuan" required="required" autofocus="autofocus">
                                        <label class="form-check-label" for="inlineRadio2">Perempuan</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="small mb-1" for="inputEmailAddress">Email</label>
                                    <input class="form-control py-4" name="email" id="inputEmailAddress" type="email" aria-describedby="emailHelp" placeholder="Masukkan alamat email" required="required" autofocus="autofocus" />
                                </div>
                                <div class="form-group mt-4 mb-0">
                                    <button class="btn btn-primary btn-block" type="submit" name="submit">Tambahkan</button>
                                    <a href="../admin/data_pelanggan.php" class="btn btn-secondary btn-block">batalkan</a>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </main>
<?php
    include('../include/footer.html');
?>