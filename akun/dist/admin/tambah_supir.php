<?php
include('../include/headeradmin.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include '../include/connect.php';

if(isset($_POST['submit'])) {
    $nama=$_POST['nama'];
    $nik=$_POST['nik'];
    $no_hp=$_POST['no_hp'];
    $email=$_POST['email'];
    $alamat = $_POST['alamat'];
    $status=$_POST['status'];
    //validasi NIK dan nohp Numeric
if($nik!=is_numeric($nik) && $no_hp!=is_numeric($no_hp)){
    echo '<script>
      alert("NIK/No. Hp harus berupa angka");
      window.location.href ="tambah_supir.php";
    </script>';
    die();
  }
  //check apakah ada NIK yang sama???
  $result=mysqli_query($conn,"SELECT nik FROM supir WHERE nik='$nik'");
  if(mysqli_fetch_assoc($result)){
    echo '<script>
      alert("Nomor Induk Kependudukan Sudah Terdaftar");
      window.location.href ="tambah_supir.php";
    </script>';
    die();
  }
//check apakah ada email yang sama???
$result=mysqli_query($conn,"SELECT email FROM supir WHERE email='$email'");
if(mysqli_fetch_assoc($result)){
  echo '<script>
    alert("Email Sudah Terdaftar");
    window.location.href ="tambah_supir.php";
  </script>';
  die();
}

    $query="INSERT INTO supir (id,nama,nik,no_hp,email,alamat,status) VALUES (null,'$nama','$nik','$no_hp','$email','$alamat','$status')";
    $result=mysqli_query($conn, $query) or die (mysqli_error($conn));
    var_dump($result);
    if(mysqli_affected_rows($conn) > 0){
      echo '<script>
         alert("Berhasil ditambahkan")
         window.location.href ="data_supir.php";
      </script>';
      }else{
            echo '<script>
                alert("Gagal ditambahkan")
                window.location.href ="tambah_supir.php";
            </script>';
    }    
}
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Tambah supir</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="data_supir.php">Data supir</a></li>
                            <li class="breadcrumb-item active">Tambah supir</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                            <form method="post">
                                <div class="form-group">
                                    <label class="small mb-1" for="inputFullName">Name Lengkap</label>
                                    <input class="form-control py-4" name="nama" id="inputFullName" type="text" placeholder="Masukkan nama lengkap" required="required" autofocus="autofocus" />
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputNIK">NIK</label>
                                            <input class="form-control py-4" name="nik" id="inputNIK" type="text" placeholder="Masukkan NIK" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputnohp">No. HP</label>
                                            <input class="form-control py-4" name="no_hp" id="inputnohp" type="text" placeholder="Masukkan No. HP" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputemail">email</label>
                                            <input class="form-control py-4" name="email" id="inputemail" type="email" placeholder="Masukkan Email" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="small mb-1" for="inputAlamat">Alamat</label>
                                    <textarea class="form-control" name="alamat" id="inputAlamat" rows="2" placeholder="Masukkan alamat" required="required" autofocus="autofocus"></textarea>
                                </div>
                                <input type="hidden" name="status" value="Tersedia">
                                <div class="form-group mt-4 mb-0">
                                    <button class="btn btn-primary btn-block" type="submit" name="submit">Tambah</button>
                                    <a href="../admin/data_supir.php" class="btn btn-secondary btn-block">batalkan</a>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </main>
<?php
    include('../include/footer.html');
?>