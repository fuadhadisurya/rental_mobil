<?php
include '../include/connect.php';

    $id = $_GET['id'];
    $result = mysqli_query($conn, "SELECT transaksi.*, mobil.harga_sewa AS harga_sewa
    FROM transaksi
    INNER JOIN mobil
    ON transaksi.id_mobil = mobil.id WHERE transaksi.id='$id'");
    $user = mysqli_fetch_array($result);

    $tgl1 = new DateTime($user['jatuh_tempo']);
    $tgl2 = new DateTime(date("Y-m-d"));
    $lama = $tgl2->diff($tgl1)->days + 1;

    $id_mobil=$user['id_mobil'];
    $id_supir=$user['id_supir'];
    $status='Tersedia';
    $status_pembayaran='Selesai';
    $tgl_kembali = date('Y-m-d');
    if($user['jatuh_tempo']<$tgl_kembali){
        $denda=$lama*$user['harga_sewa']+100000;
    }else{
        $denda=0;
    }

    $query_mobil="UPDATE mobil SET status='$status' WHERE id=$id_mobil";
    $result_mobil=mysqli_query($conn, $query_mobil) or die (mysqli_error($conn));
    
    if($id_supir!=null){
    $query_supir="UPDATE supir SET status='$status' WHERE id=$id_supir";
    $result_supir=mysqli_query($conn, $query_supir) or die (mysqli_error($conn));
    }

    $query="UPDATE transaksi SET status_pembayaran='$status_pembayaran',tgl_kembali='$tgl_kembali',denda='$denda' WHERE id=$id";
    $result=mysqli_query($conn, $query) or die (mysqli_error($conn));
    if( $result || $result_mobil || $result_supir) {
        // kalau berhasil alihkan ke halaman list-siswa.php
        echo "<script> alert('Berhasil! data telah disimpan');</script>";
        if($user['jatuh_tempo']<$tgl_kembali){
            echo "<script>window.location.assign('keterangan_denda.php?id=$id')</script>";
        }else{
            echo "<script>window.location.assign('data_peminjaman.php?id=$id')</script>";
        }
    } else {
        // kalau gagal tampilkan pesan
        echo "<script> alert('gagal diperbarui');</script>";
        echo "<script>window.location.assign('update_pengembalian.php?id=$id')</script>";
    }
?>