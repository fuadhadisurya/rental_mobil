<?php
include '../include/connect.php';
if(isset($_POST['submit'])){
    $nama=$_POST['nama'];
    $nik=$_POST['nik'];
    $kontak=$_POST['kontak'];
    $alamat=$_POST['alamat'];
    $tl=$_POST['tl']; //tempat lahir
    $tgl=$_POST['tgl']; //tanggal lahir
    $jk=$_POST['jk']; //jenis Kelamin
    $email=$_POST['email'];
    $username=$_POST['username'];
    $password1=$_POST['password1'];
    $password2=$_POST['password2'];
    //check apakah password dan konfirmasi password sama atau tidak ;
    if($password1 !==$password2){
    echo '<script>
            alert("password tidak sama");
            window.location.href ="register.php";
            </script>';
     die();
  }

//enkripsi Password
  $password1=md5($password1);
//validasi NIK dan nohp Numeric
if($nik!=is_numeric($nik) && $kontak!=is_numeric($kontak)){
    echo '<script>
      alert("NIK/No. Hp harus berupa angka");
      window.location.href ="register.php";
    </script>';
    die();
  }
//check apakah ada NIK yang sama???
  $result=mysqli_query($conn,"SELECT nik FROM user WHERE nik='$nik'");
  if(mysqli_fetch_assoc($result)){
    echo '<script>
      alert("Nomor Induk Kependudukan Sudah Terdaftar");
      window.location.href ="register.php";
    </script>';
    die();
  }
//check apakah ada email yang sama???
$result=mysqli_query($conn,"SELECT email FROM user WHERE email='$email'");
if(mysqli_fetch_assoc($result)){
  echo '<script>
    alert("Email Sudah Terdaftar");
    window.location.href ="register.php";
  </script>';
  die();
}
//check apakah ada username yang sama???
$result=mysqli_query($conn,"SELECT username FROM user WHERE username='$username'");
if(mysqli_fetch_assoc($result)){
  echo '<script>
    alert("Username Sudah Terdaftar");
    window.location.href ="register.php";
  </script>';
  die();
}
  //Query Insert data
  $query="INSERT INTO user (id,nama,email,username,password,nik,no_hp,alamat,tempat_lahir,tanggal_lahir,jenis_kelamin,level) 
  VALUES (null,'$nama','$email','$username','$password1','$nik','$kontak','$alamat','$tl','$tgl','$jk','pelanggan')";
  mysqli_query($conn,$query);
  if(mysqli_affected_rows($conn) > 0)
  {
    echo '<script>
       alert("Berhasil ditambahkan")
       window.location.href ="login.php";
    </script>';
    }else{
    echo '<script>
       alert("Gagal")
       window.location.href ="register.php";
    </script>';
  }

}//milik submit
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Register akun Wonderful travel</title>
        <link href="../css/styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header">
                                        <h3 class="text-center font-weight-light my-4">Create Account</h3>
                                    </div>
                                    <div class="card-body">
                                        <form method="post">
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputFullName">Name Lengkap</label>
                                                <input class="form-control py-4" name="nama" id="inputFullName" type="text" placeholder="Masukkan nama lengkap" required="required" autofocus="autofocus" />
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputNIK">NIK</label>
                                                        <input class="form-control py-4" name="nik" id="inputNIK" type="text" placeholder="Masukkan NIK" required="required" autofocus="autofocus" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputnohp">No. HP</label>
                                                        <input class="form-control py-4" name="kontak" id="inputnohp" type="text" placeholder="Masukkan No. HP" required="required" autofocus="autofocus" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputAlamat">Alamat</label>
                                                <textarea class="form-control" name="alamat" id="inputAlamat" rows="2" placeholder="Masukkan alamat" required="required" autofocus="autofocus"></textarea>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputTempatLahir">Tempat lahir</label>
                                                        <input class="form-control" name="tl" id="inputTempatLahir" type="text" placeholder="Masukkan tempat lahir" required="required" autofocus="autofocus" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputTanggalLahir">Tanggal lahir</label>
                                                        <input class="form-control" name="tgl" id="inputTanggalLahir" type="date" placeholder="Masukkan tanggal lahir" required="required" autofocus="autofocus" />
                                                    </div>
                                                </div>
                                            </div>
                                            <label class="small mb-1" for="inputJenisKelamin">Jenis Kelamin</label>
                                            <div class="form-row">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="jk" id="inlineRadio1" value="Laki-laki" required="required" autofocus="autofocus">
                                                    <label class="form-check-label" for="inlineRadio1">Laki-laki</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="jk" id="inlineRadio2" value="Perempuan" required="required" autofocus="autofocus">
                                                    <label class="form-check-label" for="inlineRadio2">Perempuan</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputEmailAddress">Email</label>
                                                <input class="form-control py-4" name="email" id="inputEmailAddress" type="email" aria-describedby="emailHelp" placeholder="Masukkan alamat email" required="required" autofocus="autofocus" />
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputUsernameAddress">Username</label>
                                                <input class="form-control py-4" name="username" id="inputUsernameAddress" type="text" placeholder="Masukkan Username" required="required" autofocus="autofocus" />
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputPassword">Password</label>
                                                        <input class="form-control py-4" name="password1" id="inputPassword" type="password" placeholder="Masukkan password" required="required" autofocus="autofocus" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputConfirmPassword">Password konfirmasi</label>
                                                        <input class="form-control py-4" name="password2" id="inputConfirmPassword" type="password" placeholder="Masukkan password konfirmasi" required="required" autofocus="autofocus" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group mt-4 mb-0">
                                                <button class="btn btn-primary btn-block" type="submit" name="submit">Create Account</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small">
                                            <a href="login.php">Have an account? Go to login</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                            <div class="text-muted d-flex justify-content-center">Copyright &copy; Indev 2020</div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="../js/scripts.js"></script>
    </body>
</html>
