<?php if(isset($_SESSION['username'])){ ?>
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
            <div class="sb-sidenav-menu">
                <div class="nav">
                    <div class="sb-sidenav-menu-heading">User</div>
                    <a class="nav-link" href="../admin/index.php">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-user"></i>
                        </div>
                    Profil</a>
                    <a class="nav-link" href="../admin/data_pelanggan.php">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-users"></i>
                        </div>
                    Data Pelanggan</a>
                    <a class="nav-link" href="../admin/tambah_pelanggan.php">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-plus"></i>
                        </div>
                    Tambah Pelanggan</a>
                    <div class="sb-sidenav-menu-heading">Supir</div>
                    <a class="nav-link" href="../admin/data_supir.php">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-user-tag"></i>
                        </div>
                    Data supir</a>
                    <a class="nav-link" href="../admin/tambah_supir.php">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-plus"></i>
                        </div>
                    Tambah supir</a>
                    <div class="sb-sidenav-menu-heading">Mobil</div>
                    <a class="nav-link" href="../admin/data_mobil.php">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-car"></i>
                        </div>
                    Data mobil</a>
                    <a class="nav-link" href="../admin/tambah_mobil.php">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-plus"></i>
                        </div>
                    Insert Data mobil</a>
                    <div class="sb-sidenav-menu-heading">Transaksi</div>
                    <a class="nav-link" href="../admin/data_peminjaman.php">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-list"></i>
                        </div>
                    Data peminjaman</a>
                    <a class="nav-link" href="../admin/data_pengembalian.php">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-list"></i>
                        </div>
                    Data pengembalian</a>
                    <a class="nav-link" href="../admin/pilih_mobil.php">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-plus"></i>
                        </div>
                    peminjaman OTS</a>
                    <a class="nav-link" href="../admin/konfirmasi_peminjaman.php">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-check-circle"></i>
                        </div>
                    Konfirmasi peminjaman</a>
                </div>
            </div>
            <div class="sb-sidenav-footer">
                <div class="small">Logged in as:</div>
                <?php
                if (isset($_SESSION['username'])){ ?>
                <p><?=$_SESSION['username'];?></p>
                <?php }elseif(isset($_SESSION['email'])){ ?>
                <p><?=$_SESSION['email'];?></p>
                <?php } ?>
            </div>
        </nav>
    </div>
<?php 
} 
elseif(isset($_SESSION['email'])){ 
?>
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
            <div class="sb-sidenav-menu">
                <div class="nav">
                    <div class="sb-sidenav-menu-heading">User</div>
                    <a class="nav-link" href="../user/index.php">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-user"></i>
                        </div>
                    User</a>
                    <div class="sb-sidenav-menu-heading">Sewa mobil</div>
                    <a class="nav-link" href="../user/kendaraan.php">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-car"></i>
                        </div>
                    Pilih mobil</a>
                    <a class="nav-link" href="../user/transaksi.php">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-handshake"></i>
                        </div>
                    Transaksi</a>
                    <div class="sb-sidenav-menu-heading">Transaksi</div>
                    <a class="nav-link" href="../user/riwayat.php">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-history"></i>
                        </div>
                    Riwayat Transaksi</a>
                    <a class="nav-link" href="bantuan.php">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-headset"></i>
                        </div>
                    Bantuan</a>
                </div>
            </div>
            <div class="sb-sidenav-footer">
                <div class="small">Logged in as:</div>
                <?php
                if (isset($_SESSION['username'])){ ?>
                <p><?=$_SESSION['username'];?></p>
                <?php }elseif(isset($_SESSION['email'])){ ?>
                <p><?=$_SESSION['email'];?></p>
                <?php } ?>
            </div>
        </nav>
    </div>
<?php } ?>