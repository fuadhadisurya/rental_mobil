<?php
session_start();
include '../include/connect.php';
if(isset($_POST['submit'])){
    if(isset($_SESSION['username'])){
        $username=$_POST['username'];   
    }
    elseif(isset($_SESSION['email'])){
        $email=$_POST['email'];
    }
    $passwordlama=$_POST['passwordlama'];
    $passwordlama=md5($passwordlama);
    $password1=$_POST['password1'];
    $password2=$_POST['password2'];

    //check apakah password lama cocok dengan di db
    if(isset($_SESSION['email'])){
        $result=mysqli_query($conn,"SELECT * FROM user WHERE email='$email'");
        echo '<script> alert ("Password Berhasil diganti");</script>';
        echo '<script>window.location.assign("../user/index.php")</script>';
    }
    elseif(isset($_SESSION['username'])){
        $result=mysqli_query($conn,"SELECT * FROM user WHERE username='$username'");
        echo '<script> alert ("Password Berhasil diganti");</script>';
        echo '<script>window.location.assign("../admin/index.php")</script>';
    }
    $row=mysqli_fetch_assoc($result);
    $passdb=$row['password'];
    if($passwordlama!=$passdb){
      echo '<script>
        alert("Password lama salah");
        window.location.href ="ganti_password.php";
      </script>';
      die();
    }

    //check apakah password dan konfirmasi password sama atau tidak ;
    if($password1 !==$password2){
    echo '<script>
            alert("password tidak sama");
            window.location.href ="ganti_password.php";
            </script>';
     die();
  }

    //enkripsi Password
    $password1=md5($password1);

    //Query update data
    if(isset($_SESSION['username'])){
        $query = mysqli_query($conn, "UPDATE user SET password='$password1' WHERE username = '$username'");
    }
    elseif(isset($_SESSION['email'])){
    $query = mysqli_query($conn, "UPDATE user SET password='$password1' WHERE email = '$email'");
    }

}//milik submit
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Ganti password</title>
        <link href="../css/styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header">
                                        <h3 class="text-center font-weight-light my-4">Ganti password</h3>
                                    </div>
                                    <div class="card-body">
                                        <form method="post">
                                            <div class="form-group">
                                            <?php if(isset($_SESSION['username'])){ ?>
                                                <input type="hidden" value="<?=$_SESSION['username'];?>" name="username">
                                            <?php
                                            }
                                            if(isset($_SESSION['email'])){
                                            ?>
                                                <input type="hidden" value="<?=$_SESSION['email'];?>" name="email">
                                            <?php } ?>
                                                <label class="small mb-1" for="inputPasswordLama">Password Lama</label>
                                                <input class="form-control py-4" name="passwordlama" id="inputPasswordLama" type="password" placeholder="Masukkan Password lama" required="required" autofocus="autofocus" />
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputPassword">Password</label>
                                                        <input class="form-control py-4" name="password1" id="inputPassword" type="password" placeholder="Masukkan password" required="required" autofocus="autofocus" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputConfirmPassword">Password konfirmasi</label>
                                                        <input class="form-control py-4" name="password2" id="inputConfirmPassword" type="password" placeholder="Masukkan password konfirmasi" required="required" autofocus="autofocus" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group mt-4 mb-0">
                                                <button class="btn btn-primary btn-block" type="submit" name="submit">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small">
                                        <?php if(isset($_SESSION['username'])){ ?>
                                            <a class="btn btn-secondary btn-block" href="../admin/index.php">Batalkan</a>
                                        <?php
                                        }
                                        if(isset($_SESSION['email'])){
                                        ?>
                                            <a class="btn btn-secondary btn-block" href="../user/index.php">Batalkan</a>
                                        <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                            <div class="text-muted d-flex justify-content-center">Copyright &copy; Indev 2020</div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="../js/scripts.js"></script>
    </body>
</html>
