<?php
include('../include/headeruser.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include '../include/connect.php';

if(isset($_POST['submit'])) {
    $nama=$_POST['nama'];
    $nik=$_POST['nik'];
    $kontak=$_POST['kontak'];
    $alamat=$_POST['alamat'];
    $tl=$_POST['tl']; //tempat lahir
    $tgl=$_POST['tgl']; //tanggal lahir
    $jk=$_POST['jk']; //jenis Kelamin
    $email=$_POST['email'];
    $id = $_SESSION['id'];

    $query="UPDATE user SET nama='$nama',nik='$nik',no_hp='$kontak',alamat='$alamat',tempat_lahir='$tl',tanggal_lahir='$tgl',jenis_kelamin='$jk',email='$email' WHERE id='$id'";
    $result=mysqli_query($conn, $query) or die (mysqli_error($conn));
    if( $result ) {
        // kalau berhasil alihkan ke halaman list-siswa.php
        echo '<script> alert ("Profil Berhasil diganti");</script>';
        echo '<script>window.location.assign("../user/index.php")</script>';
    } else {
        // kalau gagal tampilkan pesan
        echo '<script> alert ("Profil gagal diperbarui");</script>';
        echo '<script>window.location.assign("profil_user.php")</script>';
    }
    
}
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Ubah Profil</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="../user/index.php">Dashboard</a></li>
                            <li class="breadcrumb-item active">Ubah Profil</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                            <?php
                            //untuk menampilkan data sebelum di update/diedit
                            $id = $_SESSION['id'];
                            $result = mysqli_query($conn, "SELECT * FROM user WHERE id = '$id'");
                            $user = mysqli_fetch_array($result);
                            ?>
                            <form method="post">
                                <div class="form-group">
                                    <label class="small mb-1" for="inputFullName">Name Lengkap</label>
                                    <input class="form-control py-4" name="nama" id="inputFullName" type="text" placeholder="Masukkan nama lengkap" value="<?php echo $user['nama'] ?>" required="required" autofocus="autofocus" />
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputNIK">NIK</label>
                                            <input class="form-control py-4" name="nik" id="inputNIK" type="text" placeholder="Masukkan NIK" value="<?php echo $user['nik'] ?>" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputnohp">No. HP</label>
                                            <input class="form-control py-4" name="kontak" id="inputnohp" type="text" placeholder="Masukkan No. HP" value="<?php echo $user['no_hp'] ?>" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="small mb-1" for="inputAlamat">Alamat</label>
                                    <textarea class="form-control" name="alamat" id="inputAlamat" rows="2" placeholder="Masukkan alamat" required="required" autofocus="autofocus"><?php echo $user['alamat'] ?></textarea>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputTempatLahir">Tempat lahir</label>
                                            <input class="form-control" name="tl" id="inputTempatLahir" type="text" placeholder="Masukkan tempat lahir" value="<?php echo $user['tempat_lahir'] ?>" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputTanggalLahir">Tanggal lahir</label>
                                            <input class="form-control" name="tgl" id="inputTanggalLahir" type="date" placeholder="Masukkan tanggal lahir" value="<?php echo $user['tanggal_lahir'] ?>" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                </div>
                                <label class="small mb-1" for="inputJenisKelamin">Jenis Kelamin</label>
                                <div class="form-row">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="jk" id="inlineRadio1" value="Laki-laki" <?php if($user['jenis_kelamin']=="Laki-laki"){echo "checked";}?> required="required" autofocus="autofocus">
                                        <label class="form-check-label" for="inlineRadio1">Laki-laki</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="jk" id="inlineRadio2" value="Perempuan" <?php if($user['jenis_kelamin']=="Perempuan"){echo "checked";}?> required="required" autofocus="autofocus">
                                        <label class="form-check-label" for="inlineRadio2">Perempuan</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="small mb-1" for="inputEmailAddress">Email</label>
                                    <input class="form-control py-4" name="email" id="inputEmailAddress" type="email" aria-describedby="emailHelp" placeholder="Masukkan alamat email" value="<?php echo $user['email'] ?>" required="required" autofocus="autofocus" />
                                </div>
                                <div class="form-group mt-4 mb-0">
                                    <button class="btn btn-primary btn-block" type="submit" name="submit">Ubah</button>
                                    <a href="../user/index.php" class="btn btn-secondary btn-block">batalkan</a>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </main>
<?php
    include('../include/footer.html');
?>