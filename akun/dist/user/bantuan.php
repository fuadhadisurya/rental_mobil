<?php
include('../include/headeruser.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Bantuan</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item active">Bantuan</li>
                        </ol>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">FAQ</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                <li>Maecenas cursus ex id ipsum viverra, ut malesuada nunc tempus.</li>
                                <li>Phasellus id ante eget diam semper ultrices quis a est.</li>
                                <li>Aliquam vel diam vel eros convallis pharetra ac eget nisl.</li>
                                <li>Nunc id tellus a enim fermentum vestibulum.</li>
                                <li>Donec ut nisl aliquet, dictum sapien et, egestas ipsum.</li>
                            </div>
                        </div>

                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Kebijakan Privasi</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                                <li>Sed ac turpis sed magna posuere aliquam.</li>
                                <li>Cras sed ex quis leo maximus posuere.</li>
                                <li>Phasellus euismod dolor ac sapien tempor, in varius sapien congue.</li>
                                <li>Vestibulum sed enim cursus ligula pulvinar interdum.</li>
                            </div>
                        </div>

                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Syarat &amp; Ketentuan</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                                <li>Fusce facilisis metus vel ante accumsan sodales.</li>
                                <li>Praesent finibus nibh eu elit auctor vulputate sed a magna.</li>
                                <li>Donec at quam convallis dui sagittis tristique volutpat a lorem.</li>
                                <li>Curabitur vitae nunc a augue fringilla congue.</li>
                                <li>Nunc fermentum mi vitae metus vestibulum, in fermentum dui aliquet.</li>
                            </div>
                        </div>
                    </div>
                </main>
<?php
    include('../include/footer.html');
?>