<?php
include('../include/headeruser.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include '../include/connect.php';
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Detail transaksi</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="transaksi.php">Transaksi</a></li>
                            <li class="breadcrumb-item active">Detail transaksi</li>
                        </ol>
                        
                        <div class="card mb-4">
                            <div class="card-body">
                            <table>
                                    <?php
                                    include('../include/connect.php');
                                    if(isset($_POST['submit'])) {
                                        $id=$_POST['id'];
                                    }
                                    $query=mysqli_query($conn,"SELECT *, mobil.nama AS nama_mobil, user.nama AS nama_user
                                    FROM transaksi  INNER JOIN mobil ON transaksi.id_mobil=mobil.id INNER JOIN user ON transaksi.id_user=user.id WHERE transaksi.id='$id'");
                                    while($row=mysqli_fetch_array($query)){
                                        $harga=$row['harga_sewa'];
                                        $total=$row['total_pembayaran'];
                                        $denda=$row['denda'];
                                    ?>
                                    <tr>
                                        <td>ID Booking</td>
                                        <td> : </td>
                                        <td><?php echo $row['id'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama</td>
                                        <td> : </td>
                                        <td><?php echo $row['nama_user'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>NIK</td>
                                        <td> : </td>
                                        <td><?php echo $row['nik'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td> : </td>
                                        <td><?php echo $row['alamat'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>No. HP</td>
                                        <td> : </td>
                                        <td><?php echo $row['no_hp'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td> : </td>
                                        <td><?php echo $row['email'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Jenis kelamin</td>
                                        <td> : </td>
                                        <td><?php echo $row['jenis_kelamin'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama mobil</td>
                                        <td> : </td>
                                        <td><?php echo $row['nama_mobil'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>No. Polisi</td>
                                        <td> : </td>
                                        <td><?php echo $row['no_polisi'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal ambil</td>
                                        <td> : </td>
                                        <td><?php echo $row['tgl_sewa'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Jatuh tempo</td>
                                        <td> : </td>
                                        <td><?php echo $row['jatuh_tempo'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal kembali</td>
                                        <td> : </td>
                                        <td><?php echo $row['tgl_kembali'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Lama sewa</td>
                                        <td> : </td>
                                        <td><?php echo $row['lama'] ?> Hari</td>
                                    </tr>
                                    <tr>
                                        <td>Supir</td>
                                        <td> : </td>
                                        <td><?php echo $row['supir'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Jaminan</td>
                                        <td> : </td>
                                        <td><?php echo $row['jaminan'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Harga sewa</td>
                                        <td> : </td>
                                        <td>Rp. <?php echo number_format($harga, 0, ",",","); ?>/Hari</td>
                                    </tr>
                                    <?php if($row['supir']=='Ya'){ ?>
                                    <tr>
                                        <td>Jasa supir</td>
                                        <td> : </td>
                                        <td>Rp. 100.000/Hari</td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td>Total pembayaran</td>
                                        <td> : </td>
                                        <td>Rp. <?php echo number_format($total, 0, ",",","); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal pesan</td>
                                        <td> : </td>
                                        <td><?php echo $row['tanggal_pesan'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Denda</td>
                                        <td> : </td>
                                        <td>Rp. <?php echo number_format($denda, 0, ",",","); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Keterangan</td>
                                        <td> : </td>
                                        <td><?php echo $row['alasan'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Bukti pembayaran</td>
                                        <td> : </td>
                                    </tr>
                                </table>
                                <img src="../image/bukti_transfer/<?php echo $row['foto'] ?>" alt="Bukti Pembayaran (online user only)">
                                <?php } ?>
                            </div>
                            <div class="card-footer">
                                <a href="riwayat.php" class="btn btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
                </main>
                <script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
<?php
    include('../include/footer.html');
?>