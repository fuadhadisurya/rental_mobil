<?php
include('../include/headeruser.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Sewa mobil</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item active">Sewa mobil</li>
                        </ol>
                        <div class="d-flex justify-content-end">
                            <form method="get" class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                                <div class="input-group">
                                    <input class="form-control" type="text" name="cari" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="submit"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div style="height: 3vh;"></div>
                        <div class="row row-cols-1 row-cols-md-3">
                            <?php
                            include('../include/connect.php');
                            $halaman=12;
                            $page=isset($_GET["halaman"])?(int)$_GET["halaman"] : 1;
                            $mulai=($page>1)?($page * $halaman) - $halaman : 0;
                            $result=mysqli_query($conn,"SELECT * FROM mobil");
                            $total=mysqli_num_rows($result);
                            $pages=ceil($total/$halaman);
                            if(isset($_GET['cari'])){
                                $cari=$_GET['cari'];
                                $query=mysqli_query($conn,"SELECT * FROM mobil WHERE nama LIKE '%".$cari."%'");
                            }else{
                                $query=mysqli_query($conn,"SELECT * FROM mobil LIMIT $mulai, $halaman");
                            }
                            $no=$mulai+1;
                            while($row=mysqli_fetch_array($query)){
                                $harga=$row['harga_sewa'];
                            ?>
                            <div class="col mb-4">
                                <div class="card h-100">
                                <img src="<?php echo "../image/mobil/".$row['foto'];?>"width="200px" class="rounded mx-auto d-block">
                                    <div class="card-body">
                                        <ul class="list-group text-center">
                                            <li class="list-group"><?php echo $row['nama']?></li>
                                            <li class="list-group"><?php echo $row['transmisi']?></li>
                                            <li class="list-group"><?php echo $row['jenis']?></li>
                                            <li class="list-group"><?php echo $row['no_polisi']?></li>
                                            <li class="list-group">Rp. <?php echo number_format($harga, 0, ",",",") ?></li>
                                            <li class="list-group">
                                                <?php if($row['status']=="Tersedia"){ ?>
                                                    <span class="badge badge-success form-group">Tersedia</span>
                                                <?php
                                                } elseif($row['status']=="Tidak tersedia"){ ?>
                                                    <span class="badge badge-danger form-group">Tidak tersedia</span>
                                                <?php } ?>
                                            </li>
                                        </ul>
                                        <div class="">
                                            <a href="detail_mobil.php?id=<?php echo $row['id'] ?>" class="list-group text-center btn btn-info">Info selengkapnya</a>
                                        </div>
                                    </div>
                                    <div class="card-footer d-flex justify-content-end">
                                        <?php if($row['status']=="Tersedia"){ ?>
                                            <a class="btn btn-primary" href="sewa.php?id=<?php echo $row["id"]?>">Sewa</a>
                                        <?php
                                        } elseif($row['status']=="Tidak tersedia"){ ?>
                                            <button type="button" class="btn btn-primary" disabled>Sewa</button>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div style="height: 3vh;"></div>
                        <ol class="breadcrumb mb-4">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item"><a class="page-link" href="?halaman=<?php echo 1?>">First</a></li>
                                    <?php for($i=1;$i<=$pages;$i++){ ?>
                                        <li class="page-item"><a class="page-link" href="?halaman=<?php echo $i?>"><?php echo $i?></a></li>
                                    <?php } ?>
                                    <li class="page-item"><a class="page-link" href="?halaman=<?php echo $pages?>">Last</a></li>
                                </ul>
                            </nav>
                        </ol>
                    </div>
                </main>
                <script>
                $(document).ready(function() {
                    $('#mobil').DataTable();
                } );
                </script>
<?php
    include('../include/footer.html');
?>