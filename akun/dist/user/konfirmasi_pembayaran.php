<?php
include('../include/headeruser.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include '../include/connect.php';

if(isset($_POST['submit'])) {
    $id=$_POST['id'];
    $status = $_POST['status'];
    $foto_tipe =strtolower($_FILES['gambar']['type']);
    $foto_name =$_FILES['gambar']['name'];
    $foto_size =$_FILES['gambar']['size'];
    $foto_temp =$_FILES['gambar']['tmp_name'];
    $foto_error =$_FILES['gambar']['error'];

    //hapus gambar lama
if($foto_name==true){
    $result=mysqli_query($conn,"SELECT foto FROM transaksi WHERE id='$id'");
    $row=mysqli_fetch_assoc($result);
    unlink("../image/bukti_transfer/".$row['foto']);
}

    move_uploaded_file($foto_temp,'../image/bukti_transfer/'.$foto_name);
    if($foto_name==""){
        $query="UPDATE transaksi SET status_pembayaran='$status' WHERE id='$id'";
    }else{
        $query="UPDATE transaksi SET status_pembayaran='$status',foto='$foto_name' WHERE id='$id'";
    }

    $result=mysqli_query($conn, $query) or die (mysqli_error($conn));
    var_dump($query);
    if( $result ) {
        // kalau berhasil alihkan ke halaman list-siswa.php
        echo '<script> alert ("Berhasil diupload");</script>';
        echo '<script>window.location.assign("upload_sukses.php?id='.$id.'")</script>';
    } else {
        // kalau gagal tampilkan pesan
        echo '<script> alert ("gagal mengupload");</script>';
        echo '<script>window.location.assign("konfirmasi_pembayaran.php")</script>';
    }
}
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Konfirmasi pembayaran</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="transaksi.php">Transaksi</a></li>
                            <li class="breadcrumb-item active">Konfirmasi pembayaran</li>
                        </ol>
                        
                        <div class="card mb-4">
                            <div class="card-body">
                            <table>
                                    <?php
                                    include('../include/connect.php');
                                    $id=$_GET['id'];
                                    $query=mysqli_query($conn,"SELECT * FROM transaksi WHERE id='$id'");
                                    while($row=mysqli_fetch_array($query)){
                                        $total=$row['total_pembayaran'];
                                    ?>
                                    <tr>
                                        <td>ID Booking</td>
                                        <td> : </td>
                                        <td><?php echo $row['id'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama</td>
                                        <td> : </td>
                                        <td><?=$_SESSION['nama'];?></td>
                                    </tr>
                                    <tr>
                                        <td>No. HP</td>
                                        <td> : </td>
                                        <td><?=$_SESSION['no_hp'];?></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td> : </td>
                                        <td><?=$_SESSION['email'];?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal ambil</td>
                                        <td> : </td>
                                        <td><?php echo $row['tgl_sewa'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Jatuh tempo</td>
                                        <td> : </td>
                                        <td><?php echo $row['jatuh_tempo'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Supir</td>
                                        <td> : </td>
                                        <td><?php echo $row['supir'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Lama sewa</td>
                                        <td> : </td>
                                        <td><?php echo $row['lama'] ?> Hari</td>
                                    </tr>
                                    <tr>
                                        <td>Jaminan</td>
                                        <td> : </td>
                                        <td><?php echo $row['jaminan'] ?></td>
                                    </tr>
                                    
                                    <tr>
                                        <td>Total pembayaran</td>
                                        <td> : </td>
                                        <td>Rp. <?php echo number_format($total, 0, ",",","); ?></td>
                                    </tr>
                                </table>
                                <?php } ?>
                            <?php
                            //untuk menampilkan data sebelum di update/diedit
                            $id = $_GET['id'];
                            $result = mysqli_query($conn, "SELECT * FROM transaksi WHERE id = '$id'");
                            $data = mysqli_fetch_array($result);
                            ?>
                                <form method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label class="small mb-1" for="inputGambar">Upload bukti pembayaran</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="gambar" name="gambar" required="required" autofocus="autofocus">
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                        </div>
                                    </div>
                                    <input type="hidden" name="id" value="<?php echo $data['id'] ?>">
                                    <input type="hidden" value="Menunggu konfirmasi" name="status">
                                <button class="btn btn-primary btn-block" type="submit" name="submit">Upload</button>
                                <a href="transaksi.php" class="btn btn-secondary btn-block">Kembali</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </main>
                <script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
<?php
    include('../include/footer.html');
?>