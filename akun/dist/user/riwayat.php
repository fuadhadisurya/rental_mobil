<?php
include('../include/headeruser.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Transaksi</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item active">Transaksi</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>Transaksi
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="transaksi" width="100%" cellspacing="0">
                                        <thead>
                                            <tr class="text-center">
                                                <th>ID Booking</th>
                                                <th>Nama</th>
                                                <th>Tanggal ambil</th>
                                                <th>Jatuh tempo</th>
                                                <th>Lama</th>
                                                <th>Jaminan</th>
                                                <th>Total pembayaran</th>
                                                <th>Status pembayaran</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                include('../include/connect.php');
                                                $iduser=$_SESSION['id'];
                                                $query=mysqli_query($conn,"SELECT transaksi.id, mobil.nama, transaksi.tgl_sewa, transaksi.jatuh_tempo, transaksi.lama, transaksi.supir, transaksi.jaminan, mobil.harga_sewa, transaksi.total_pembayaran, transaksi.status_pembayaran
                                                FROM transaksi INNER JOIN mobil ON transaksi.id_mobil=mobil.id INNER JOIN user ON transaksi.id_user=user.id WHERE transaksi.id_user='$iduser' AND status_pembayaran='Selesai' OR status_pembayaran='Ditolak'");
                                                while($row=mysqli_fetch_array($query)){
                                                    $harga=$row['harga_sewa'];
                                                    $total=$row['total_pembayaran'];
                                            ?>
                                            <tr>
                                                <td><?php echo $row['id'] ?></td>
                                                <td><?php echo $row['nama'] ?></td>
                                                <td><?php echo $row['tgl_sewa'] ?></td>
                                                <td><?php echo $row['jatuh_tempo'] ?></td>
                                                <td><?php echo $row['lama'] ?></td>
                                                <td><?php echo $row['jaminan'] ?></td>
                                                <td>Rp. <?php echo number_format($total, 0, ",",",") ?></td>
                                                <td>
                                                    <?php if($row['status_pembayaran']=="Selesai"){ ?>
                                                        <span class="badge badge-primary">Selesai</span>
                                                    <?php } elseif($row['status_pembayaran']=="Ditolak"){ ?>
                                                        <span class="badge badge-secondary">Ditolak</span>
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <form method="post" action="detail_transaksi.php">
                                                        <input type="hidden" name="id" value="<?php echo $row["id"]?>">
                                                        <button class="btn btn-info" type="submit" name="submit"><i class="fas fa-info-circle"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <script>
                $(document).ready(function() {
                    $('#transaksi').DataTable();
                } );
                </script>
<?php
    include('../include/footer.html');
?>