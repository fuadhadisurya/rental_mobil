<?php
include('../include/headeruser.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include '../include/connect.php';

$id=$_GET['id'];
$query=mysqli_query($conn,"SELECT * FROM mobil WHERE id='$id'");
while($row=mysqli_fetch_array($query)){
if($row['status']=="Tersedia"){
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Konfirmasi sewa</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="kendaraan.php">Sewa mobil</a></li>
                            <li class="breadcrumb-item active">Konfirmasi sewa</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-body">
                                <?php
                                include('../include/connect.php');
                                //untuk menampilkan data sebelum di update/diedit
                                $id = $_GET['id'];
                                $result = mysqli_query($conn, "SELECT * FROM mobil WHERE id = '$id'");
                                $mobil = mysqli_fetch_array($result);
                                $harga=$mobil['harga_sewa'];
                                ?>
                                <form action="sewa_sukses.php" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id_mobil" value="<?php echo $mobil['id'] ?>">
                                <div class="form-row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputNama">Nama Mobil</label>
                                            <input class="form-control py-4" name="nama" id="inputNama" type="text" value="<?php echo $mobil['nama'] ?>" required="required" autofocus="autofocus" disabled />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputNopol">No. Polisi</label>
                                            <input class="form-control py-4" name="no_polisi" id="inputNopol" type="text" value="<?php echo $mobil['no_polisi'] ?>" required="required" autofocus="autofocus" disabled/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputJenis">Jenis</label>
                                            <input class="form-control" name="jenis" id="inputJenis" type="text" value="<?php echo $mobil['jenis'] ?>" required="required" autofocus="autofocus" disabled/>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputTransmisi">Transmisi</label>
                                            <input class="form-control" name="transmisi" id="inputTransmisi" type="text" value="<?php echo $mobil['transmisi'] ?>" required="required" autofocus="autofocus" disabled/>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputHarga">Harga sewa</label>
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">Rp. </div>
                                                </div>
                                                <input class="form-control" name="harga_curr" id="inputHarga" type="text" value="<?php echo number_format($harga, 0, ",",","); echo "/Hari" ?>" required="required" autofocus="autofocus" disabled/>
                                                <input type="hidden" value="<?php echo $mobil['harga_sewa'] ?>" name="harga">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputSewa">Tanggal ambil</label>
                                            <input class="form-control" name="tgl_sewa" id="inputSewa" type="date" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputTempo">Tanggal kembali</label>
                                            <input class="form-control" name="jatuh_tempo" id="inputTempo" type="date" required="required" autofocus="autofocus" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="small mb-1" for="inputSupir">Dengan supir?</label>
                                    <select class="form-control" id="inputSupir" name="supir" required="required" autofocus="autofocus">
                                        <option>Ya</option>
                                        <option>Tidak</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="small mb-1" for="inputJaminan">Jaminan</label>
                                    <select class="form-control" id="inputJaminan" name="jaminan" required="required" autofocus="autofocus">
                                        <option>STNK</option>
                                        <option>Sertifikat rumah</option>
                                    </select>
                                </div>
                                <input type="hidden" value="Tidak tersedia" name="status">
                                <input type="hidden" value="Belum dibayarkan" name="status_pembayaran">
                                <div class="form-group mt-4 mb-0">
                                    <button class="btn btn-primary btn-block" type="submit" name="submit">Sewa</button>
                                    <a href="../user/kendaraan.php" class="btn btn-secondary btn-block">Batalkan</a>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </main>
<?php
    include('../include/footer.html');
}
else{
    echo '<script>
                window.location.href ="kendaraan.php";
            </script>';
} }?>