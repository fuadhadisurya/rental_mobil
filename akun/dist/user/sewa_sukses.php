<?php
include('../include/headeruser.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include '../include/connect.php';

if(isset($_POST['submit'])) {
    $id_mobil=$_POST['id_mobil'];
    $status = $_POST['status'];
    $tgl_sewa= $_POST['tgl_sewa'];
    $jatuh_tempo=$_POST['jatuh_tempo'];
    $supir=$_POST['supir'];
    $status_pembayaran=$_POST['status_pembayaran'];
    $jaminan=$_POST['jaminan'];
    $harga=$_POST['harga'];
    $id_user = $_SESSION['id'];

    $tgl1 = new DateTime($_POST['tgl_sewa']);
    $tgl2 = new DateTime($_POST['jatuh_tempo']);
    $lama = $tgl2->diff($tgl1)->days + 1;
    
    if($supir=="Ya"){
        $jasa_supir=100000;
        $total=($harga+$jasa_supir)*$lama;
    }else{
        $total=$harga*$lama;
    }

    $query_mobil="UPDATE mobil SET status='$status' WHERE id='$id_mobil'";
    $result_mobil=mysqli_query($conn, $query_mobil) or die (mysqli_error($conn));

    $query_transaksi="INSERT INTO transaksi (id,id_user,id_mobil,supir,tgl_sewa,jatuh_tempo,lama,status_pembayaran,jaminan,total_pembayaran,tanggal_pesan) 
    VALUES (null,'$id_user','$id_mobil','$supir','$tgl_sewa','$jatuh_tempo','$lama','$status_pembayaran','$jaminan','$total',NOW())";
    $result_transaksi=mysqli_query($conn, $query_transaksi) or die (mysqli_error($conn));
    if( $result_transaksi && $result_mobil) {
        // kalau berhasil alihkan ke halaman list-siswa.php
        
    } else {
        // kalau gagal tampilkan pesan
        echo '<script> alert ("Gagal, coba ulangi lagi");</script>';
        echo '<script>window.location.assign("sewa.php")</script>';
    }
}

?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Sewa Sukses</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="kendaraan.php">Sewa mobil</a></li>
                            <li class="breadcrumb-item active">Konfirmasi mobil</a></li>
                            <li class="breadcrumb-item active">Sewa sukses</li>
                        </ol>

                        <div class="card mb-4">
                            <div class="card-header">
                                Data booking
                            </div>
                            <div class="card-body">
                                <table>
                                    <?php
                                    include('../include/connect.php');
                                    $query=mysqli_query($conn,"SELECT * FROM transaksi ORDER BY id DESC LIMIT 1");
                                    while($row=mysqli_fetch_array($query)){
                                    ?>
                                    <tr>
                                        <td>ID Booking</td>
                                        <td> : </td>
                                        <td><?php echo $row['id'] ?></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td>Nama</td>
                                        <td> : </td>
                                        <td><?=$_SESSION['nama'];?></td>
                                    </tr>
                                    <tr>
                                        <td>No. HP</td>
                                        <td> : </td>
                                        <td><?=$_SESSION['no_hp'];?></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td> : </td>
                                        <td><?=$_SESSION['emaildb'];?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal ambil</td>
                                        <td> : </td>
                                        <td><?php echo $tgl_sewa ?></td>
                                    </tr>
                                    <tr>
                                        <td>Jatuh tempo</td>
                                        <td> : </td>
                                        <td><?php echo $jatuh_tempo ?></td>
                                    </tr>
                                    <tr>
                                        <td>Supir</td>
                                        <td> : </td>
                                        <td><?php echo $supir ?></td>
                                    </tr>
                                    <tr>
                                        <td>Lama sewa</td>
                                        <td> : </td>
                                        <td><?php echo $lama ?> Hari</td>
                                    </tr>
                                    <tr>
                                        <td>Jaminan</td>
                                        <td> : </td>
                                        <td><?php echo $jaminan ?></td>
                                    </tr>
                                    <tr>
                                        <td>Harga sewa mobil</td>
                                        <td> : </td>
                                        <td>Rp. <?php echo number_format($harga, 0, ",",",") ?>/Hari</td>
                                    </tr>
                                    <?php if($supir=='Ya'){ ?>
                                    <tr>
                                        <td>Jasa supir</td>
                                        <td> : </td>
                                        <td>Rp. 100.000/Hari</td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td>Total pembayaran</td>
                                        <td> : </td>
                                        <td>Rp. <?php echo number_format($total, 0, ",",","); ?></td>
                                    </tr>
                                </table>
                                <hr>
                                <h3>Terimakasih</h3>
                                <p>Transaksi pembelian anda telah berhasil<br>
                                Silahkan anda membayar tagihan anda dengan cara transfer via Bank BRI di nomor Rekening : <br>
                                <strong>(0986-01-025805-53-8 a/n SEWA MOBIL)</strong> untuk menyelesaikan pembayaran. dan untuk uang muka minimal setengah dari harga sewa.
                                </p>
                                <p>
                                Jika anda sudah melakukan transfer silahkan anda melakukan konfirmasi pembayaran dengan mengunjungi halaman transaksi anda lalu tekan tombol. <i><b>yang bergambar upload</b></i>.
                                </p>
                                <p> Batas Konfirmasi 1 jam, jika lebih dari 1 jam anda tidak melakukan konfirmasi maka sistem akan membatalkan pesanan secara otomatis.</p>
                            </div>
                            <div class="card-footer">
                                <a href="transaksi.php" class="btn btn-primary">Tutup</a>
                            </div>
                        </div>
                    </div>
                </main>
<?php
    include('../include/footer.html');
?>