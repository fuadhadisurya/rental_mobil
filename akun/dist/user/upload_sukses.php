<?php
include('../include/headeruser.php');
include('../include/logoheader.php');
include('../include/sidebar.php');
include '../include/connect.php';

if(isset($_POST['submit'])) {
    $id=$_POST['id'];
    $status = $_POST['status'];
    $foto_tipe =strtolower($_FILES['gambar']['type']);
    $foto_name =$_FILES['gambar']['name'];
    $foto_size =$_FILES['gambar']['size'];
    $foto_temp =$_FILES['gambar']['tmp_name'];
    $foto_error =$_FILES['gambar']['error'];

    //hapus gambar lama
if($foto_name==true){
    $result=mysqli_query($conn,"SELECT foto FROM transaksi WHERE id='$id'");
    $row=mysqli_fetch_assoc($result);
    unlink("../image/bukti_transfer/".$row['foto']);
}

    move_uploaded_file($foto_temp,'../image/bukti_transfer/'.$foto_name);
    if($foto_name==""){
        $query="UPDATE transaksi SET status_pembayaran='$status' WHERE id='$id'";
    }else{
        $query="UPDATE transaksi SET status_pembayaran='$status',foto='$foto_name' WHERE id='$id'";
    }

    $result=mysqli_query($conn, $query) or die (mysqli_error($conn));

    if( $result ) {
        // kalau berhasil alihkan ke halaman list-siswa.php
        echo '<script> alert ("Berhasil diupload");</script>';
        echo '<script>window.location.assign("upload_sukses.php?")</script>';
    } else {
        // kalau gagal tampilkan pesan
        echo '<script> alert ("gagal mengupload");</script>';
        echo '<script>window.location.assign("konfirmasi_pembayaran.php")</script>';
    }
}
?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Sewa Sukses</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="transaksi.php">Transaksi</a></li>
                            <li class="breadcrumb-item active">Konfirmasi Pembayaran</li>
                            <li class="breadcrumb-item active">Upload sukses</li>
                        </ol>

                        <div class="card mb-4">
                            <div class="card-header">
                                Data booking
                            </div>
                            <div class="card-body">
                                <table>
                                    <?php
                                    include('../include/connect.php');
                                    $id=$_GET['id'];
                                    $query=mysqli_query($conn,"SELECT transaksi.*, mobil.harga_sewa AS harga_sewa FROM transaksi INNER JOIN mobil ON transaksi.id_mobil = mobil.id WHERE transaksi.id=$id");
                                    while($row=mysqli_fetch_array($query)){
                                    $supir=$row['jaminan'];
                                    $harga=$row['harga_sewa'];
                                    $total=$row['total_pembayaran'];
                                    ?>
                                    <tr>
                                        <td>ID Booking</td>
                                        <td> :  </td>
                                        <td><?php echo $row['id'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama</td>
                                        <td> : </td>
                                        <td><?=$_SESSION['nama'];?></td>
                                    </tr>
                                    <tr>
                                        <td>No. HP</td>
                                        <td> : </td>
                                        <td><?=$_SESSION['no_hp'];?></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td> : </td>
                                        <td><?=$_SESSION['emaildb'];?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal ambil</td>
                                        <td> : </td>
                                        <td><?php echo $row['tgl_sewa'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Jatuh tempo</td>
                                        <td> : </td>
                                        <td><?php echo $row['jatuh_tempo'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Supir</td>
                                        <td> : </td>
                                        <td><?php echo $row['supir'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Lama sewa</td>
                                        <td> : </td>
                                        <td><?php echo $row['lama'] ?> Hari</td>
                                    </tr>
                                    <tr>
                                        <td>Jaminan</td>
                                        <td> : </td>
                                        <td><?php echo $row['jaminan'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Harga sewa mobil</td>
                                        <td> : </td>
                                        <td>Rp. <?php echo number_format($harga, 0, ",",",") ?>/Hari</td>
                                    </tr>
                                    <?php if($supir=='Ya'){ ?>
                                    <tr>
                                        <td>Jasa supir</td>
                                        <td> : </td>
                                        <td>Rp. 100.000/Hari</td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td>Total pembayaran</td>
                                        <td> : </td>
                                        <td>Rp. <?php echo number_format($total, 0, ",",","); ?></td>
                                    </tr>
                                    <?php } ?>
                                </table>
                                <hr>
                                <h3>Pesanan anda akan kami proses</h3>
                                <p>Upload bukti pembayaran anda telah berhasil<br>
                                Terima kasih telah menyelesaikan transaksi pembayaran. Silahkan anda menunggu konfirmasi pembayaran oleh admin<br></p>
                                <p>Jika masih ada pertanyaan/kendala silahkan hubungi kami</p>
                            </div>
                            <div class="card-footer">
                                <a href="transaksi.php" class="btn btn-primary">Tutup</a>
                            </div>
                        </div>
                    </div>
                </main>
<?php
    include('../include/footer.html');
?>