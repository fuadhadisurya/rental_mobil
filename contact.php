<?php
	include('head.html');
	include('logoheader.html');
?>
			<!-- start banner Area -->
			<section class="banner-area relative" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								Contact Us				
							</h1>	
							<p class="text-white link-nav"><a href="index.html">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="contact.html"> Contact Us</a></p>
						</div>	
					</div>
				</div>
			</section>
			<!-- End banner Area -->				  

			<!-- Start contact-page Area -->
			<section class="contact-page-area section-gap">
				<div class="container">
					<div class="row">
						<!-- <div class="map-wrap" style="width:100%; height: 445px;" id="map"></div> -->
						<div class="map-wrap" style="width:100%; height: 445px;" id="mapid"></div>
						<div class="container">
<div class="container">
  	<div class="row justify-content-center">
    	<div class="col-4">
			<div class="single-contact-address d-flex flex-row">
				<div class="icon">
					<span class="lnr lnr-home"></span>
				</div>
				<div class="contact-details">
					<h5>Indramayu, Indonesia</h5>
					<p>Jl.Raya Lohbener Lama No.08 Lohbener</p>
				</div>
			</div>
    	</div>
    	<div class="col-3">
			<div class="single-contact-address d-flex flex-row">
				<div class="icon">
					<span class="lnr lnr-phone-handset"></span>
				</div>
				<div class="contact-details">
					<h5>0234 123 456</h5>
					<p>Setiap Hari 09.00-16.00</p>
				</div>
			</div>
    	</div>
    	<div class="col-4">
			<div class="single-contact-address d-flex flex-row">
				<div class="icon">
					<span class="lnr lnr-envelope"></span>
				</div>
				<div class="contact-details">
					<h5>support@wonderfultravel.com</h5>
					<p>Kirim Pesan Kapan Aja!</p>
				</div>
			</div>
    	</div>
  	</div>
</div>
						<!-- <div class="col-lg-4 d-flex flex-column address-wrap">
							<div class="single-contact-address d-flex flex-row">
								<div class="icon">
									<span class="lnr lnr-home"></span>
								</div>
								<div class="contact-details">
									<h5>Indramayu, Indonesia</h5>
									<p>Jl.Raya Lohbener Lama No.08 Lohbener</p>
								</div>
							</div>
							<div class="single-contact-address d-flex flex-row">
								<div class="icon">
									<span class="lnr lnr-phone-handset"></span>
								</div>
								<div class="contact-details">
									<h5>0234 123 456</h5>
									<p>Setiap Hari 09.00-16.00</p>
								</div>
							</div>
							<div class="single-contact-address d-flex flex-row">
								<div class="icon">
									<span class="lnr lnr-envelope"></span>
								</div>
								<div class="contact-details">
									<h5>support@wonderfultravel.com</h5>
									<p>Kirim Pesan Kapan Aja!</p>
								</div>
							</div>														
						</div> -->
						<!-- <div class="col-lg-8">
							<form class="form-area " id="myForm" action="mail.php" method="post" class="contact-form text-right">
								<div class="row">	
									<div class="col-lg-6 form-group">
										<input name="name" placeholder="Enter your name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" class="common-input mb-20 form-control" required="" type="text">
									
										<input name="email" placeholder="Enter email address" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'" class="common-input mb-20 form-control" required="" type="email">

										<input name="subject" placeholder="Enter your subject" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your subject'" class="common-input mb-20 form-control" required="" type="text">
										<div class="mt-20 alert-msg" style="text-align: left;"></div>
									</div>
									<div class="col-lg-6 form-group">
										<textarea class="common-textarea form-control" name="message" placeholder="Messege" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Messege'" required=""></textarea>
										<button class="genric-btn primary circle mt-30" style="float: right;">Send Message</button>								
									</div>
								</div>
							</form>	
						</div> -->
					</div>
				</div>	
			</section>
			<!-- End contact-page Area -->
<?php
	include('footer.html');
?>


