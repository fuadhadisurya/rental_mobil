<?php
	include('head.html');
	include('logoheader.html');
?>
			<!-- start banner Area -->
			<section class="banner-area relative" id="home">
				<div class="overlay overlay-bg"></div>
				<div class="container">
					<div class="row fullscreen d-flex align-items-center justify-content-center">
						<div class="banner-content col-lg-12 col-md-12">
							<h6 class="text-uppercase">Don’t look further, here is the key</h6>
							<h1>
								We’re Transportation solution			
							</h1>
							<p class="text-white">
							Menyediakan beberapa jasa kebutuhan transportasi Seperti Pensewaan kendaraan, Paket berlibur,<br> Tiket Pesawat, Tiket Kereta Api, dan lain sebagainya.
							</p>
							<a href="/akun/dist/auth/pilih.php" class="primary-btn header-btn text-uppercase">Mulai</a>
						</div>												
					</div>
				</div>
			</section>
			<!-- End banner Area -->

			<!-- Start cat Area -->
			<section class="cat-area section-gap aboutus-cat" id="feature">
				<div class="container">							
					<div class="row">
						<div class="col-lg-4">	
							<div class="single-cat d-flex flex-column">
								<a href="#" class="hb-sm-margin mx-auto d-block"><span class="hb hb-sm inv hb-facebook-inv"><span class="lnr lnr-cog"></span></span></a>
								<h4 class="mb-20" style="margin-top: 23px;">Armada Mobil</h4>
								<p>
								Kami selalu melakukan perawatan secara berkala pada armada mobil kami, untuk menjaga kenyamanan berkendara anda selama diperjalanan.
								</p>
							</div>															
						</div>
						<div class="col-lg-4">	
							<div class="single-cat">
								<a href="#" class="hb-sm-margin mx-auto d-block"><span class="hb hb-sm inv hb-facebook-inv"><span class="lnr lnr-star"></span></span></a>
								<h4 class="mt-40 mb-20">Quality Over Value</h4>
								<p>
								Kepuasan anda adalah Prioritas kami.
								</p>
							</div>															
						</div>
						<div class="col-lg-4">
							<div class="single-cat">
								<a href="#" class="hb-sm-margin mx-auto d-block"><span class="hb hb-sm inv hb-facebook-inv"><span class="lnr lnr-car"></span></span></a>
								<h4 class="mt-40 mb-20">Mobil yang Bervariasi</h4>
								<p>
								Menyediakan beberapa pilihan mobil yang menyesuaikan dengan kebutuhan rental dan keuangan anda.
								</p>
							</div>							
						</div>
					</div>
				</div>	
			</section>
			<!-- End cat Area -->							
			

			<!-- Start home-about Area -->
			<section class="home-about-area section-gap" id="about">
				<div class="container">
					<div class="row justify-content-center align-items-center">
						<div class="col-lg-8 col-md-12 home-about-left">
							<h1>
								Sekilas Wonderful Travel
							</h1>
							<p class="pb-20">
							Wonderful Travel merupakan salah satu penyedia layanan rental mobil di 
								Indramayu. Menggunakan armada mobil keluaran baru, dengan kondisi yang sangat 
								terawat untuk disewakan kepada Anda. Pilihan mobil yang kami sediakan 
								sangat bervariatif dan harga yang sangat kompetitif di Indramayu.
							</p>
							<a class="primary-btn" href="about.php">Baca Selengkapnya</a>
						</div>
					</div>
				</div>	
			</section>
			<!-- End home-about Area -->
					
			<!-- Start service Area -->
			<section class="service-area section-gap" id="service">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-12 pb-30 header-text text-center">
							<h1 class="mb-10">Mobil Kami</h1>
							<p>
								Silahkan Pilih Mobil yang akan Disewakan
							</p>
						</div>
					</div>
					<div style="height: 3vh;"></div>
					<div class="row row-cols-1 row-cols-md-3">
                        <?php
                            include('akun/dist/include/connect.php');
                            $query=mysqli_query($conn,"SELECT * FROM mobil LIMIT 3");
                            while($row=mysqli_fetch_array($query)){
                                $harga=$row['harga_sewa'];
                        ?>
                        <div class="col-lg-4">
							<div class="single-service">
								<div class="thumb">
									<img src="<?php echo "akun/dist/image/mobil/".$row['foto'];?>"width="200px" height="250px" class="rounded mx-auto d-block">									
								</div>
								<h4 class="text-center"><?php echo $row['nama']?></h4>
								<ul class="list-group text-center">
                                    <li class="list-group"><?php echo $row['transmisi']?></li>
                                    <li class="list-group"><?php echo $row['jenis']?></li>
                                    <li class="list-group"><?php echo $row['no_polisi']?></li>
                                    <li class="list-group">Rp. <?php echo number_format($harga, 0, ",",",") ?></li>
                                    <li class="list-group">
									<?php 
										if($row['status']=="Tersedia"){ 
									?>
                                	<span class="badge badge-success form-group">Tersedia</span>
									<?php } 
										elseif($row['status']=="Tidak tersedia"){ 
									?>
                                        <span class="badge badge-danger form-group">Tidak tersedia</span>
                                    <?php } ?>
                                    </li>
                                </ul>
							</div>
						</div>
                    	<?php } ?>
					</div>
				</div>
			</section>		
			<!-- End service Area -->
			

			<!-- Start faq Area -->
			<section class="faq-area section-gap relative">
				<div class="overlay overlay-bg"></div>
				<div class="container">
					<div class="row justify-content-center align-items-center">
					<?php
                        include('akun/dist/include/connect.php');
                    ?>
						<div class="col-lg-3 col-md-6">
							<div class="single-faq">
								<div class="circle">
								  <div class="inner"></div>
								</div>
								<h5 span class="counter">
									<?php 
										$total_user=mysqli_query($conn,"SELECT * FROM user");
										echo mysqli_num_rows($total_user);
									?>
								</h5>
								<p>
									Total Pelanggan 
								</p>
							</div>
						</div>
						<div class="col-lg-3 col-md-6">
							<div class="single-faq">
								<div class="circle">
								  <div class="inner"></div>
								</div>
								<h5 class="counter">
									<?php 
										$total_mobil=mysqli_query($conn,"SELECT * FROM mobil");
										echo mysqli_num_rows($total_mobil);
									?>
								</h5>
								<p>
									Total Mobil
								</p>
							</div>
						</div>
						<div class="col-lg-3 col-md-6">
							<div class="single-faq">
								<div class="circle">
								  <div class="inner"></div>
								</div>
								<h5 class="counter">
									<?php 
										$mobil_tersedia=mysqli_query($conn,"SELECT * FROM mobil WHERE status='Tersedia'");
										echo mysqli_num_rows($mobil_tersedia);
									?>
								</h5>
								<p>
									Mobil Tersedia
								</p>
							</div>
						</div>
						<div class="col-lg-3 col-md-6">
							<div class="single-faq">
								<div class="circle">
								  <div class="inner"></div>
								</div>
								<h5 class="counter">
									<?php 
										$transaksi=mysqli_query($conn,"SELECT * FROM transaksi WHERE status_pembayaran='Selesai'");
										echo mysqli_num_rows($transaksi);
									?>
								</h5>
								<p>
									Transaksi Berhasil
								</p>
							</div>
						</div>																		
					</div>
				</div>	
			</section>
			<!-- End faq Area -->


			<!-- Start project Area -->
			<section class="project-area section-gap" id="project">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-8 pb-30 header-text text-center">
							<h1 class="mb-10">Transaksi</h1>
							<p>
								Transaksi dengan pelanggan
							</p>
						</div>
					</div>						
					<div class="row">
						<div class="col-lg-8 col-md-8">
							<a href="img/p1.jpg" class="img-gal">
								<img class="img-fluid single-project" src="img/p1.jpg" alt="">
							</a>	
						</div>
						<div class="col-lg-4 col-md-4">
							<a href="img/p2.jpg" class="img-gal">
								<img class="img-fluid single-project" src="img/p2.jpg" alt="">
							</a>	
						</div>						
						<div class="col-lg-6 col-md-6">
							<a href="img/p3.jpg" class="img-gal">
								<img class="img-fluid single-project" src="img/p3.jpg" alt="">
							</a>	
						</div>
						<div class="col-lg-6 col-md-6">
							<a href="img/p4.jpg" class="img-gal">
								<img class="img-fluid single-project" src="img/p4.jpg" alt="">
							</a>	
						</div>		
					</div>
				</div>	
			</section>
			<!-- End project Area -->
						
			<!-- Start feedback Area -->
			<section class="feedback-area section-gap relative" id="feedback">
				<div class="overlay overlay-bg"></div>
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-12 pb-30 header-text text-center">
							<h1 class="mb-10 text-white">Testimoni dari pelanggan</h1>
							<p class="text-white">
							Kepuasan Anda adalah Prioritas Kami
							</p>
						</div>
					</div>			
					<div class="row feedback-contents justify-content-center align-items-center">
						<div class="col-lg-6 feedback-left relative d-flex justify-content-center align-items-center">
							<div class="overlay overlay-bg"></div>
							<a class="play-btn" href="https://www.youtube.com/watch?v=ARA0AxrnHdM"><img class="img-fluid" src="img/play-btn.png" alt=""></a>
						</div>
						<div class="col-lg-6 feedback-right">
							<div class="active-review-carusel">
								<div class="single-feedback-carusel">
									<div class="title d-flex flex-row">
										<h4 class="text-white pb-10">Putri Yulianti</h4>
										<div class="star">
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>								
										</div>										
									</div>
									<p class="text-white">
										Terimakasih Wonderful Travel. saya dan sekeluarga bisa berlibur dan berkeliling di 
										Indramayu, Besok kalau saya butuh lagi akan saya pilih Wonderful Travel 
										dan pastinya saya akan rekomendasikan ke teman-teman
									</p>
								</div>
								<div class="single-feedback-carusel">
									<div class="title d-flex flex-row">
										<h4 class="text-white pb-10">Radika Kurniawan</h4>
										<div class="star">
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>								
										</div>										
									</div>
									<p class="text-white">
										Saya sudah lama berlangganan Wonderful Travel. Harganya 
										bersahabat, mobil bersih dan nyaman, supirnya ramah-ramah. 
										Saya puas sekali. Terima kasih, Wonderful Travel.
									</p>
								</div>
								<div class="single-feedback-carusel">
									<div class="title d-flex flex-row">
										<h4 class="text-white pb-10">Nadia Purwanti</h4>
										<div class="star">
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked	"></span>								
										</div>										
									</div>
									<p class="text-white">
									Senang pakai jasa rental Wonderful Travel. Banyak pilihan 
									paketnya, pelayanan sangat memuaskan. Besok kalau ke 
									Indramayu lagi, saya akan sewa lagi.
									</p>
								</div>																
							</div>
						</div>
					</div>
				</div>	
			</section>
			<!-- End feedback Area -->
			
	
			<!-- Start blog Area -->
			<!-- <section class="blog-area section-gap" id="blog">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-12 pb-30 header-text">
							<h1>Latest posts from our Blog</h1>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> labore  et dolore magna aliqua.
							</p>
						</div>
					</div>
					<div class="row">	
						<div class="single-blog col-lg-4 col-md-4">
							<div class="thumb">
								<img class="f-img img-fluid mx-auto" src="img/b1.jpg" alt="">	
							</div>
							<div class="bottom d-flex justify-content-between align-items-center flex-wrap">
								<div>
									<img class="img-fluid" src="img/user.png" alt="">
									<a href="#"><span>Mark Wiens</span></a>
								</div>
								<div class="meta">
									13th Dec
									<span class="lnr lnr-heart"></span> 15
									<span class="lnr lnr-bubble"></span> 04
								</div>
							</div>							
							<a href="#">
								<h4>Portable Fashion for young women</h4>
							</a>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore  et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea.
							</p>
						</div>
						<div class="single-blog col-lg-4 col-md-4">
							<div class="thumb">
								<img class="f-img img-fluid mx-auto" src="img/b2.jpg" alt="">	
							</div>
							<div class="bottom d-flex justify-content-between align-items-center flex-wrap">
								<div>
									<img class="img-fluid" src="img/user.png" alt="">
									<a href="#"><span>Mark Wiens</span></a>
								</div>
								<div class="meta">
									13th Dec
									<span class="lnr lnr-heart"></span> 15
									<span class="lnr lnr-bubble"></span> 04
								</div>
							</div>							
							<a href="#">
								<h4>Portable Fashion for young women</h4>
							</a>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore  et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea.
							</p>
						</div>
						<div class="single-blog col-lg-4 col-md-4">
							<div class="thumb">
								<img class="f-img img-fluid mx-auto" src="img/b3.jpg" alt="">	
							</div>
							<div class="bottom d-flex justify-content-between align-items-center flex-wrap">
								<div>
									<img class="img-fluid" src="img/user.png" alt="">
									<a href="#"><span>Mark Wiens</span></a>
								</div>
								<div class="meta">
									13th Dec
									<span class="lnr lnr-heart"></span> 15
									<span class="lnr lnr-bubble"></span> 04
								</div>
							</div>							
							<a href="#">
								<h4>Portable Fashion for young women</h4>
							</a>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore  et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea.
							</p>
						</div>												
											
												
					</div>
				</div>	
			</section> -->
			<!-- end blog Area -->	
				
<?php
include('footer.html');
?>