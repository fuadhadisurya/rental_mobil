<?php
	include('head.html');
	include('logoheader.html');
?>
			<!-- start banner Area -->
			<section class="banner-area relative" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								Services				
							</h1>	
							<p class="text-white link-nav"><a href="index.html">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="services.html"> Services</a></p>
						</div>	
					</div>
				</div>
			</section>
			<!-- End banner Area -->	
				
			<!-- Start service Area -->
			<section class="service-area section-gap" id="service">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-12 pb-30 header-text text-center">
							<h1 class="mb-10">Mobil Kami</h1>
							<p>
								Silahkan Pilih Mobil yang akan Disewakan
							</p>
						</div>
					</div>
					<div class="d-flex justify-content-end">
                        <form method="get" class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                            <div class="input-group">
                                <input class="form-control" type="text" name="cari" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit"><span class="lnr lnr-magnifier"></span></button>
                                </div>
                            </div>
                        </form>
                    </div>	
					<div style="height: 3vh;"></div>
					<div class="row row-cols-1 row-cols-md-3">
                        <?php
                            include('akun/dist/include/connect.php');
                            $halaman=9;
                            $page=isset($_GET["halaman"])?(int)$_GET["halaman"] : 1;
                            $mulai=($page>1)?($page * $halaman) - $halaman : 0;
                            $result=mysqli_query($conn,"SELECT * FROM mobil");
                            $total=mysqli_num_rows($result);
                            $pages=ceil($total/$halaman);
                            if(isset($_GET['cari'])){
                                $cari=$_GET['cari'];
                                $query=mysqli_query($conn,"SELECT * FROM mobil WHERE nama LIKE '%".$cari."%'");
                            }else{
                                $query=mysqli_query($conn,"SELECT * FROM mobil LIMIT $mulai, $halaman");
                            }
                            $no=$mulai+1;
                            while($row=mysqli_fetch_array($query)){
                                $harga=$row['harga_sewa'];
                        ?>
                        <div class="col-lg-4">
							<div class="single-service">
								<div class="thumb">
									<img src="<?php echo "akun/dist/image/mobil/".$row['foto'];?>"width="200px" height="250px" class="rounded mx-auto d-block">									
								</div>
								<h4 class="text-center"><?php echo $row['nama']?></h4>
								<ul class="list-group text-center">
                                    <li class="list-group"><?php echo $row['transmisi']?></li>
                                    <li class="list-group"><?php echo $row['jenis']?></li>
                                    <li class="list-group"><?php echo $row['no_polisi']?></li>
                                    <li class="list-group">Rp. <?php echo number_format($harga, 0, ",",",") ?></li>
                                    <li class="list-group">
									<?php 
										if($row['status']=="Tersedia"){ 
									?>
                                	<span class="badge badge-success form-group">Tersedia</span>
									<?php } 
										elseif($row['status']=="Tidak tersedia"){ 
									?>
                                        <span class="badge badge-danger form-group">Tidak tersedia</span>
                                    <?php } ?>
                                    </li>
                                </ul>
							</div>
						</div>
                    	<?php } ?>
					</div>
				</div>
			</section>		
			<!-- End service Area -->
			
            <div style="height: 3vh;"></div>
            <ol class="breadcrumb mb-4">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link" href="?halaman=<?php echo 1?>">First</a></li>
                        <?php for($i=1;$i<=$pages;$i++){ ?>
                        <li class="page-item"><a class="page-link" href="?halaman=<?php echo $i?>"><?php echo $i?></a></li>
                        <?php } ?>
                        <li class="page-item"><a class="page-link" href="?halaman=<?php echo $pages?>">Last</a></li>
                    </ul>
                </nav>
            </ol>          
            <script>
            $(document).ready(function() {
                $('#mobil').DataTable();
            } );
            </script>
<?php
include('footer.html');
?>


